import {MENU} from '../../../loctors/menu'
describe('Membership Fees.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Update Membership Fees -Nominal case ',()=>{
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link').click();
        cy.get('#\\36 66808451482405935 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();

        cy.get('#myreg_ig_grid_vc_cur > a').click()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','450px')
        cy.handleModal()
        cy.getIframeDom('#P101015_CMF_IDEN').clear().type('842')
        cy.getIframeDom('#P101015_CMF_LABE').clear().type('VISA Membership Fees')
        cy.getIframeDom('#P101015_CMF_AMOU').clear().type('1000.05')

        /** *********************************************************************************** */
        cy.getIframeDom('#P101015_CMF_PER_CODE_lov_btn').click()
        cy.get('#PopupLov_101015_P101015_CMF_PER_CODE_dlg > div.a-PopupLOV-searchBar > input')
        .type('annuel{enter}')
        cy.get('#PopupLov_101015_P101015_CMF_PER_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste1').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste1').first().children().first().click()
            }
          })

          /************************************************************************************** */
          cy.getIframeDom('#P101015_CMF_BAN_CODE_lov_btn').click()
          cy.get('#PopupLov_101015_P101015_CMF_BAN_CODE_dlg > div.a-PopupLOV-searchBar > input')
          .type('BICICI{enter}')
          cy.get('#PopupLov_101015_P101015_CMF_BAN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste2').first().then(($ul) => {
              // Vérifiez si l'élément ul existe
              if ($ul.length) {
                // Sélectionnez le premier enfant de ul
                cy.get('@liste2').first().children().first().click()
              }
            })

            /*********************************************************************************** */

            cy.getIframeDom('#B87042929780351111').should('be.visible').click()
            cy.get('#t_Alert_Success').should('be.visible')

    })
})

/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
describe('Card Parameter.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Update Card Remplacement Reason -Nominal case ',()=>{
        
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#\\36 66871638801445837 > :nth-child(3) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#myreg_ig_grid_vc > div.a-GV-bdy > div.a-GV-w-scroll > table > tbody > tr:nth-child(3) > td.a-GV-cell.u-tC.is-readonly > a').click()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height', '300px')
        cy.handleModal()
        cy.getIframeDom('#P101025_CRM_BAN_CODE_lov_btn').click()
        cy.get('#PopupLov_101025_P101025_CRM_BAN_CODE_dlg > div.a-PopupLOV-searchBar > input')
        .type('BICICI{enter}')

        cy.get('#PopupLov_101025_P101025_CRM_BAN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste').first().children().first().click()
            }
          })
        cy.getIframeDom('#P101025_CRM_IDEN').clear().type('001')
        cy.getIframeDom('#P101025_CRM_LABE').clear().type('CARTE VOLEE')
        cy.getIframeDom('#B229912975472806545').should('be.visible').click()
        cy.get('#t_Alert_Success').should('be.visible')

    })
})
/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
describe('Personalization Fees .',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Update Personalization Fees -Nominal case ',()=>{
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#\\36 66808451482405935 > :nth-child(3) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#myreg_ig_grid_vc_cur > a').click()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','380px')
        cy.handleModal()

        cy.getIframeDom('#P101011_PFE_BAN_CODE_lov_btn').click()
        cy.get('#PopupLov_101011_P101011_PFE_BAN_CODE_dlg > div.a-PopupLOV-searchBar > input')
        .type('BICICI{enter}')
        cy.get('#PopupLov_101011_P101011_PFE_BAN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste').first().children().first().click()
            }
          })

        cy.getIframeDom('#P101011_PFE_IDEN').clear().type('001')
        cy.getIframeDom('#P101011_PFE_LABE').clear().type('lable')
        cy.getIframeDom('#P101011_PFE_AMOU').clear().type('85')
        cy.getIframeDom('#B86840850555009467').should('be.visible').click()
        cy.get('#t_Alert_Success').should('be.visible')
    })

    it.only('Update Personalization Fees-Mandatory fields left empty -Exceptional case ',()=>{
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#\\36 66808451482405935 > :nth-child(3) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#myreg_ig_grid_vc_cur > a').click()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','380px')
        cy.handleModal()

        cy.getIframeDom('#P101011_PFE_BAN_CODE_lov_btn').click()
        cy.get('#PopupLov_101011_P101011_PFE_BAN_CODE_dlg > div.a-PopupLOV-searchBar > input')
        .type('BICICI{enter}')
        cy.get('#PopupLov_101011_P101011_PFE_BAN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste').first().children().first().click()
            }
          })

        cy.getIframeDom('#P101011_PFE_IDEN').clear()
        cy.getIframeDom('#P101011_PFE_LABE').clear()
        cy.getIframeDom('#P101011_PFE_AMOU').clear()
        cy.getIframeDom('#B86840850555009467').should('be.visible').click()
        cy.get('#t_Alert_Notification').should('be.visible')
    })
})
import {MENU} from '../../../loctors/menu'
describe('Bin Opposition Reason.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Add Bin Opposition Reason-Nominal case ',()=>{
       
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#\\36 66871638801445837 > :nth-child(6) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('table').find('tbody tr').then(($rows) => {
            const  rowCount = $rows.length;
            console.log(rowCount)
        cy.get('.t-Button-label').click();

        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height', '300px')
        cy.handleModal()
        const id = Math.floor(Math.random() * 100) + 1;
        cy.getIframeDom("#P101037_BOM_IDEN").type(id)
        cy.getIframeDom("#P101037_BOM_LABE").type('Blacklist')
        cy.getIframeDom('#B304300570364934329').should('be.visible').click()
        cy.get('table').find('tbody tr').should('have.length', rowCount + 1);
    })
    })
})
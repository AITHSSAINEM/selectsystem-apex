/// <reference types ="cypress" />
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import { ACCESS} from "../../../loctors/bank/bank";
import { Commission, Revolving, Settlement} from "../../../loctors/bank/configure";

describe('Create BANK', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        // Step 01 :  Access
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(ACCESS.BANK_MANAGEMENT_ITEM).click()
        cy.get(Commission.PENCIL).click();
    })

    it('Add Bank\'s Settlement Account - Nominal Case', ()=>{
        cy.get(Settlement.LINK).click();
        cy.get(Settlement.ADD_ROW_BTN).click();
        cy.get(Settlement.IFRAME).invoke('css' , 'height','250px')
        cy.handleModal()
        cy.getIframeDom(Settlement.MXP_ACCOUT).type('9486746549485')
        cy.getIframeDom(Settlement.BANK_ACCOUNT).type('37627667575')
        cy.getIframeDom(Settlement.CURRENCY).select('39')
        cy.getIframeDom(Settlement.CREATE_BTN).should('be.visible').and('have.text', 'Create')

        // Pour ajouter une compte de règlement bancaire, il suffit d'activer la ligne suivante : 
         cy.getIframeDom(Settlement.CREATE_BTN).click()
    })

    
})
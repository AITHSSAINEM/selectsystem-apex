
/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Acquirer Risk Management Parameters.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    })

    it('Configure Acquirer Risk Management Parameters - Nominal case.', ()=>{
        cy.get('#\\35 23400178171740781 > :nth-child(2) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#R182223024764852463_ig_grid_vc_cur > a > :nth-child(2)').click();
        cy.get('#B128788111905191878 > .t-Button-label').click();
        cy.get('#R164005304074253823_tab > a > span').click();
        cy.get('#P900212_ARM_SYST_CARD_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_ARM_CARD_TYPE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_ARM_CARD_TYPE_NETW_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_ARM_PAYM_CHAN_CONTAINER > .t-Form-inputContainer').click();
        cy.get('#P900212_ARM_PAYM_CHAN_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_ARM_SOLV_LEVE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_ARM_SYST_CARD_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_ARM_CARD_TYPE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_ARM_CARD_TYPE_NETW_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_ARM_PAYM_CHAN_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_ARM_SOLV_LEVE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#B128814243583200840 > .t-Button-label').should('be.visible');
        cy.get('#B128814243583200840').click()
    })
})

/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Bank Acquirer Parameters.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    })

    it('Configure Merchant Commission Parameters -Nominal case-.', ()=>{
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#\\35 23400178171740781 > :nth-child(2) > .t-LinksList-link').click();
        cy.get('#R182223024764852463_ig_grid_vc_cur > a > :nth-child(2)').click();
        cy.get('#B128788111905191878 > .t-Button-label').click();
        cy.get('#R164005154689253821_tab > a > span').click();
        cy.get('#P900212_CTY_TRAN_CODE').clear('7');
        cy.get('#P900212_CTY_TRAN_CODE').type('7');
        cy.get('#P900212_CTY_MCC_GROU').clear();
        cy.get('#P900212_CTY_MCC_GROU').type('6');
        cy.get('#P900212_CTY_MERC_COUN').clear();
        cy.get('#P900212_CTY_MERC_COUN').type('5');
        cy.get('#P900212_CTY_PCA_CODE').clear();
        cy.get('#P900212_CTY_PCA_CODE').type('4');
        cy.get('#P900212_CTY_SYS_CODE').clear();
        cy.get('#P900212_CTY_SYS_CODE').type('3');
        cy.get('#P900212_CTY_CARD_USAG').clear();
        cy.get('#P900212_CTY_CARD_USAG').type('2');
        cy.get('#P900212_CTY_BTY_CODE').clear();
        cy.get('#P900212_CTY_BTY_CODE').type('1');
        cy.get('#B128814243583200840 > .t-Button-label').should('be.visible');
        /* ==== End Cypress Studio ==== */
    })
})
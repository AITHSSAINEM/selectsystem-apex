/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import {DEBIT_APP} from '../../../loctors/debitApplication'
describe('Debit Management ',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.ISSUING).click()
    })
     
    it('Debit application list ',()=>{
      cy.get(DEBIT_APP.APP_MANAGEMENT_SECTION).should('be.visible')
      cy.get(DEBIT_APP.DEBIT_APP_BUTTON).contains('Debit Application').click()
      cy.get(DEBIT_APP.LIST_DEBIT_APP).should('be.visible')
      .find('tr')
      .first()
      .find('th')
      .should('have.have.length.greaterThan',0)
    })

})
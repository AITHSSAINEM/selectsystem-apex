/// <reference types="cypress"/>
import {MENU} from '../../loctors/menu'
import { NEW_CENTER , ADDRESS , UI_DIALOG, FIRS_CONTACT, SEC_CONTACT, CORRESPENDANCE } from '../../loctors/center'
describe('Add Center',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
    })

    it(' Test Add Center - Nominal case ', ()=>{
      // Step 01 : CENTER 
      
      cy.get(NEW_CENTER.CENTER_ID).clear('');
      cy.get(NEW_CENTER.CENTER_ID).type('0001');
      cy.get(NEW_CENTER.CENTER_NAME).clear('');
      cy.get(NEW_CENTER.CENTER_NAME).type('BNP COTE D’IVOIRE');
      cy.get(NEW_CENTER.CENTER_BTN_SEAR).click();
      cy.get(NEW_CENTER.CENTER_INP_SEAR).clear();
      cy.get(NEW_CENTER.CENTER_INP_SEAR).type('950 - CFA Franc BEAC{enter}');
      cy.get(NEW_CENTER.LIST_REF).first().then(($ul) => {
          // Vérifiez si l'élément ul existe
          if ($ul.length) {
            // Sélectionnez le premier enfant de ul
            cy.get(NEW_CENTER.LIST_REF).first().children().first().click()
          }
        })
      cy.get(NEW_CENTER.CENTER_INITIAL).clear();
      cy.get(NEW_CENTER.CENTER_INITIAL).type('BICICI');
      cy.get(NEW_CENTER.LICENCE_NUM).clear();
      cy.get(NEW_CENTER.LICENCE_NUM).type('123456111111111');
      cy.get(NEW_CENTER.SOCIAL_NUM).clear();
      cy.get(NEW_CENTER.SOCIAL_NUM).type('123451111116789');
      cy.get(NEW_CENTER.SERIE_NUM).clear();
      cy.get(NEW_CENTER.SERIE_NUM).type('111111111111');
      cy.get(NEW_CENTER.CAPITAL).clear();
      cy.get(NEW_CENTER.CAPITAL).type('1111111120000000000');
      cy.get(NEW_CENTER.COM_REGI_NUM).clear('');
      cy.get(NEW_CENTER.COM_REGI_NUM).type('2');
      cy.get(NEW_CENTER.TIME_ZONE_SEL).select('M');
      cy.get(NEW_CENTER.TIME_ZONE_INP).clear();
      cy.get(NEW_CENTER.TIME_ZONE_INP).type('3');

      //Step 02 : Les assertions pour les champs d'adresses

      cy.get(ADDRESS.ADDRESS_TYPE).should('be.visible');
      cy.get(ADDRESS.ADDRESS1).should('be.visible');
      cy.get(ADDRESS.ADDRESS2).should('be.visible');
      cy.get(ADDRESS.CITY).should('be.visible');
      cy.get(ADDRESS.PHONE).should('be.visible');
      cy.get(ADDRESS.FAX).should('be.visible');

      //Step 03 : Add new address 
      cy.get(ADDRESS.ADD_BTN).click()
      cy.get("#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable").invoke('css', 'height', '800px')
      cy.handleModal()
      cy.getIframeDom(UI_DIALOG.TYPE_BTN).click()
      cy.get(UI_DIALOG.TYPE_INP).type("01 - Mail Address{enter}")
      cy.get(UI_DIALOG.TYPE_LIST).first().then(($ul) => {
        // Vérifiez si l'élément ul existe
        if ($ul.length) {
          // Sélectionnez le premier enfant de ul
          cy.get(UI_DIALOG.TYPE_LIST).first().children().first().click()
        }
      })
      cy.getIframeDom(UI_DIALOG.ADDRESS).type('103 Zerktouni Avenue')
      cy.getIframeDom(UI_DIALOG.ADDRESS2).type('5th floor, Door 5')
      cy.getIframeDom(UI_DIALOG.STREET).type('Abidjan Street')
      cy.getIframeDom(UI_DIALOG.PO_BOX).type('54646')
      cy.getIframeDom(UI_DIALOG.ZIP).type('45100')
      cy.getIframeDom(UI_DIALOG.CITY_BTN).click()
      cy.get(UI_DIALOG.CITY_INP).type("Abidjan{enter}")
      cy.get(UI_DIALOG.CITY_LIST).first().then(($ul) => {
        // Vérifiez si l'élément ul existe
        if ($ul.length) {
          // Sélectionnez le premier enfant de ul
          cy.get(UI_DIALOG.CITY_LIST).first().children().first().click()
        }
      })
      cy.getIframeDom(UI_DIALOG.STATE).type('384')
      cy.getIframeDom(UI_DIALOG.PHONE1).type('0198765432')
      cy.getIframeDom(UI_DIALOG.PHONE2).type('0198765473')
      cy.getIframeDom(UI_DIALOG.FAX).type('0150765444')
      cy.getIframeDom('#R225546316643469027').should('be.visible')

      // remplir les champs des deux contacts.

      // First Conctact 

      cy.getIframeDom(FIRS_CONTACT.FIRS_NAME).type('Ahmed')
      cy.getIframeDom(FIRS_CONTACT.LAST_NAME).type('Ali')
      cy.getIframeDom(FIRS_CONTACT.FUNCTION).type('Agent')
      cy.getIframeDom(FIRS_CONTACT.PHONE).type('0165478498')
      cy.getIframeDom(FIRS_CONTACT.EMAIL).type('ahmed.ali@bicici.ci')

      // Second Contact
     cy.getIframeDom(SEC_CONTACT.FIRS_NAME).type('Mostapha')
     cy.getIframeDom(SEC_CONTACT.LAST_NAME).type('Mohamed')
     cy.getIframeDom(SEC_CONTACT.FUNCTION).type('Agent')
     cy.getIframeDom(SEC_CONTACT.PHONE).type('0165478498')
     cy.getIframeDom(SEC_CONTACT.EMAIL).type('m.mohamed@bicici.ci')
     cy.getIframeDom(UI_DIALOG.CREATE_BTN).should('be.visible')
    
     //Pour générer une nouvelle adresse, il vous suffit d'activer la ligne suivante : 
     //cy.getIframeDom(UI_DIALOG.CREATE_BTN).should('be.visible').click()
      cy.getIframeDom('#B226367318058288381').click()

      //  Step 04 : add new correspendance 

      cy.get(CORRESPENDANCE.ADD_BTN).click();
      cy.get(CORRESPENDANCE.TYPE_CORR).select('2',{force: true});
      cy.get('#R225546586182469029_ig_grid_vc_cur').click({force: true});
      cy.get(CORRESPENDANCE.ADDRESS_TYPE).select('4',{force: true});
      cy.get('#R225546586182469029_ig_grid_vc_cur').click({force: true});
      cy.get(CORRESPENDANCE.MODE_COM).type('E-mail',{force: true});
      // to save
      cy.get(CORRESPENDANCE.SAVE_BTN).click()

      
      // Step 05 & 06 :
      cy.get('#P900201_CEN_SETT_CURR_CODE_CONTAINER > .t-Form-inputContainer').click();
      cy.get('#P900201_CEN_SETT_CURR_CODE_lov_btn').click()
      cy.get('#PopupLov_900201_P900201_CEN_SETT_CURR_CODE_dlg > div.a-PopupLOV-searchBar > input').clear().type('950 - CFA Franc BEAC{enter}');
      cy.get('#PopupLov_900201_P900201_CEN_SETT_CURR_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').first().then(($ul) => {
        // Vérifiez si l'élément ul existe
        if ($ul.length) {
          // Sélectionnez le premier enfant de ul
          cy.get('#PopupLov_900201_P900201_CEN_SETT_CURR_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').first().children().first().click()
        }
      })
      cy.get('#P900201_CEN_ACC_NUMB_MXP').clear('MXP999999999999999CNTCFA');
      cy.get('#P900201_CEN_ACC_NUMB_MXP').type('678451316513');
      cy.get('#P900201_CEN_ACC_NUMB').click();
      cy.get('#P900201_CEN_ACC_NUMB').clear('897645654486462');
      cy.get('#P900201_CEN_ACC_NUMB').type('897645654486462');
      cy.get('#P900201_CEN_CUT_OFF_PERI').select('21');
      cy.get('#P900201_CEN_CUT_OFF_TIME').click();
      cy.get('#P900201_CEN_CUT_OFF_TIME_CONTAINER > .t-Form-inputContainer').click();
      cy.get('#P900201_CEN_CUT_OFF_TIME').clear().type('17:00')
      cy.get('#P900201_CEN_LAST_CUT_OFF_input').click().clear().type('2022-10-20');

      cy.get('#B223995386427068629').scrollIntoView().should('be.visible').and('have.text', 'Apply Changes')

      //Pour appliquer ces modifications, il suffit d'activer la ligne suivante : 
      //cy.get('#B223995386427068629').click()
    })


    /*
    it.skip('Add new address', () => {
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
        cy.get(ADDRESS.ADD_BTN).click()
        cy.get("#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable").invoke('css', 'height', '800px')
        cy.handleModal()
        cy.getIframeDom(UI_DIALOG.TYPE_BTN).click()
        cy.get(UI_DIALOG.TYPE_INP).type("01 - Mail Address{enter}")
        cy.get(UI_DIALOG.TYPE_LIST).first().then(($ul) => {
          // Vérifiez si l'élément ul existe
          if ($ul.length) {
            // Sélectionnez le premier enfant de ul
            cy.get(UI_DIALOG.TYPE_LIST).first().children().first().click()
          }
        })
        cy.getIframeDom(UI_DIALOG.ADDRESS).type('103 Zerktouni Avenue')
        cy.getIframeDom(UI_DIALOG.ADDRESS2).type('5th floor, Door 5')
        cy.getIframeDom(UI_DIALOG.STREET).type('Abidjan Street')
        cy.getIframeDom(UI_DIALOG.PO_BOX).type('54646')
        cy.getIframeDom(UI_DIALOG.ZIP).type('45100')
        cy.getIframeDom(UI_DIALOG.CITY_BTN).click()
        cy.get(UI_DIALOG.CITY_INP).type("Abidjan{enter}")
        cy.get(UI_DIALOG.CITY_LIST).first().then(($ul) => {
          // Vérifiez si l'élément ul existe
          if ($ul.length) {
            // Sélectionnez le premier enfant de ul
            cy.get(UI_DIALOG.CITY_LIST).first().children().first().click()
          }
        })
        cy.getIframeDom(UI_DIALOG.STATE).type('384')
        cy.getIframeDom(UI_DIALOG.PHONE1).type('0198765432')
        cy.getIframeDom(UI_DIALOG.PHONE2).type('0198765473')
        cy.getIframeDom(UI_DIALOG.FAX).type('0150765444')
        cy.getIframeDom('#R225546316643469027').should('be.visible')
        // remplir les champs des deux contacts.
        
        // First Conctact 

        cy.getIframeDom(FIRS_CONTACT.FIRS_NAME).type('Ahmed')
        cy.getIframeDom(FIRS_CONTACT.LAST_NAME).type('Ali')
        cy.getIframeDom(FIRS_CONTACT.FUNCTION).type('Agent')
        cy.getIframeDom(FIRS_CONTACT.PHONE).type('0165478498')
        cy.getIframeDom(FIRS_CONTACT.EMAIL).type('ahmed.ali@bicici.ci')
       
        // Second Contact
        cy.getIframeDom(SEC_CONTACT.FIRS_NAME).type('Mostapha')
        cy.getIframeDom(SEC_CONTACT.LAST_NAME).type('Mohamed')
        cy.getIframeDom(SEC_CONTACT.FUNCTION).type('Agent')
        cy.getIframeDom(SEC_CONTACT.PHONE).type('0165478498')
        cy.getIframeDom(SEC_CONTACT.EMAIL).type('m.mohamed@bicici.ci')
        cy.getIframeDom(UI_DIALOG.CREATE_BTN).should('be.visible')

    })

    it.skip('Add new correspendance', () => {
      cy.get(NEW_CENTER.BASE_PARAM).click();
      cy.get(NEW_CENTER.CENTER_ITEM).click();

      cy.get(CORRESPENDANCE.ADD_BTN).click();
      cy.get(CORRESPENDANCE.TYPE_CORR).select('2',{force: true});
      cy.get('#R225546586182469029_ig_grid_vc_cur').click({force: true});
      cy.get(CORRESPENDANCE.ADDRESS_TYPE).select('4',{force: true});
      cy.get('#R225546586182469029_ig_grid_vc_cur').click({force: true});
      cy.get(CORRESPENDANCE.MODE_COM).type('E-mail',{force: true});
      // to save
      cy.get(CORRESPENDANCE.SAVE_BTN).click()
    })


    it('Add new parameter', () => {
      cy.get(NEW_CENTER.BASE_PARAM).click();
      cy.get(NEW_CENTER.CENTER_ITEM).click();
      
      cy.get('#P900201_CEN_SETT_CURR_CODE_CONTAINER > .t-Form-inputContainer').click();
      cy.get('#P900201_CEN_SETT_CURR_CODE_lov_btn').click()
      cy.get('.a-PopupLOV-search').clear('950 - CFA Franc BEAC');
      cy.get('.a-PopupLOV-search').type('950 - CFA Franc BEAC');
      cy.get('.a-PopupLOV-searchBar > .a-Button > .a-Icon').click();
      cy.get('.popup-lov-highlight').click();
      cy.get('#P900201_CEN_ACC_NUMB_MXP').clear('MXP999999999999999CNTCFA');
      cy.get('#P900201_CEN_ACC_NUMB_MXP').type('678451316513');
      cy.get('#P900201_CEN_ACC_NUMB').click();
      cy.get('#P900201_CEN_ACC_NUMB').clear('897645654486462');
      cy.get('#P900201_CEN_ACC_NUMB').type('897645654486462');
      cy.get('#P900201_CEN_CUT_OFF_PERI').select('21');
      cy.get('#P900201_CEN_CUT_OFF_TIME').click();
      cy.get('#P900201_CEN_CUT_OFF_TIME_CONTAINER > .t-Form-inputContainer').click();
      cy.get('#P900201_CEN_CUT_OFF_TIME').clear().type('17:00')
      cy.get('#P900201_CEN_LAST_CUT_OFF_input').click().clear().type('2022-10-20');
      
    })

    */

      
})
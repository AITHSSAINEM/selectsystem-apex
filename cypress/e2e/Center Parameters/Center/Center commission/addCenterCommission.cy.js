/// <reference types="cypress"/>
import {MENU} from '../../../../loctors/menu'
import { NEW_CENTER , ADDRESS , UI_DIALOG, FIRS_CONTACT, SEC_CONTACT, CORRESPENDANCE } from '../../../../loctors/center'
describe('Center Commission ',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
    })

    it(' Add center commission  - Nominal case ', ()=>{
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#myreg_data_tab > a > span').should('be.visible').and('have.text','Center Menu').click()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(2) > a').should('be.visible').contains('Center Commission').click()
        cy.get('#mybtn').click();
        cy.get('#P100302_COM_IDEN').type('10005');
        cy.get('#P100302_COM_RATE').type('1.2');
        cy.get('#P100302_COM_MIN').type('10');
        cy.get('#P100302_COM_LABE').type('Interchange Fees');
        cy.get('#P100302_COM_FIXE').type('20');
        cy.get('#P100302_COM_MAX').type('30');
        cy.get('.t-Button-label').should('be.visible');
        cy.get('#B234442319461367453').should('have.text', 'Add New Commission').click();
        cy.get('.t-Alert-title').should('be.visible');
        cy.get('.t-Alert-title').should('have.text', 'Changes saved');
    })

    it.only(' Add center commission -Exceptional case ', ()=>{
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#myreg_data_tab > a > span').should('be.visible').and('have.text','Center Menu').click()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(2) > a').should('be.visible').contains('Center Commission').click()
        cy.get('#mybtn').click();
        cy.get('#P100302_COM_IDEN').clear();
        cy.get('#P100302_COM_RATE').type('1.2');
        cy.get('#P100302_COM_MIN').type('10');
        cy.get('#P100302_COM_LABE').type('Interchange Fees');
        cy.get('#P100302_COM_FIXE').type('20');
        cy.get('#P100302_COM_MAX').type('30');
        cy.get('.t-Button-label').should('be.visible');
        cy.get('#B234442319461367453').should('have.text', 'Add New Commission').click();
       
    })
})
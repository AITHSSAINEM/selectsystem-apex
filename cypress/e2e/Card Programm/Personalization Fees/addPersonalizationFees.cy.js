/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
describe('Personalization Fees .',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Add Personalization Fees -Nominal case ',()=>{
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        
        cy.get('#\\36 66808451482405935 > :nth-child(3) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#mybtn').click();

        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','380px')
        cy.handleModal()

        cy.getIframeDom('#P101011_PFE_BAN_CODE_lov_btn').click()
        cy.get('#PopupLov_101011_P101011_PFE_BAN_CODE_dlg > div.a-PopupLOV-searchBar > input')
        .type('BICICI{enter}')
        cy.get('#PopupLov_101011_P101011_PFE_BAN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste').first().children().first().click()
            }
          })

        cy.getIframeDom('#P101011_PFE_IDEN').type('001')
        cy.getIframeDom('#P101011_PFE_LABE').type('lable')
        cy.getIframeDom('#P101011_PFE_AMOU').type('85')
        
        cy.getIframeDom('#B86841262349009468').should('be.visible').click()
        cy.get('#t_Alert_Success').should('be.visible')
        
        
    })
})
/// <reference types="cypress"/>
import {MENU} from '../../../../loctors/menu'
import { NEW_CENTER , ADDRESS , UI_DIALOG, FIRS_CONTACT, SEC_CONTACT, CORRESPENDANCE } from '../../../../loctors/center'
describe('Center currency rate ',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
    })

    it(' update currency rate  - Nominal case ', ()=>{
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#myreg_data_tab > a > span').should('be.visible').and('have.text','Center Menu').click()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(1) > a').click()
        cy.get('#R233923540248182925_ig_grid_vc > div.a-GV-bdy > div.a-GV-w-scroll > table > tbody > tr:nth-child(7) > td:nth-child(1) > a').click({force:true})
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css' ,'height', '500px')
    cy.getIframeDom('#B233920958074182923').should('be.visible').and('have.text','Delete')
    })
})
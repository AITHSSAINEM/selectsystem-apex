
/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Personalization Paramters.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    })

    it('Configure Personalization Charge/Revolving. - Nominal case.', ()=>{
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#\\35 23400178171740781 > :nth-child(2) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#R182223024764852463_ig_grid_vc_cur > a > .fa').click();
        cy.get('b').click();
        cy.get('#R163648642234320819_tab > a > span').click();
        cy.get('#P900211_PPA_BRA_CODE_1_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_PPA_CARD_TYPE_NETW_1_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_PPA_BRA_CODE_1_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#B128787068630191877 > .t-Button-label').should('be.visible');
        /* ==== End Cypress Studio ==== */
    })
})
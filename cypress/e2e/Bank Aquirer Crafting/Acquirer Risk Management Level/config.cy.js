
/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Aquirer Risk Management Level..',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    })

    it('Configure Acquirer Risk Management Level - Nominal case.', ()=>{
        
        cy.get('#\\35 23400178171740781 > :nth-child(2) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#R182223024764852463_ig_grid_vc_cur > a > :nth-child(2)').click();
        cy.get('#B128788111905191878').click();
        cy.get('#R164005422950253824_tab > a > span').click();
        cy.get('#P900212_ARM_MERC_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_ARM_MERC_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#B128814243583200840 > .t-Button-label').should('be.visible');
        cy.get('#B128814243583200840').click()
        
    })

})
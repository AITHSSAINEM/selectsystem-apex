export const SCHEME = {
    BANK_BTN : "#P902211_SBA_BANK_CODE_lov_btn",
    BANK_SEARCH_INP : '#PopupLov_902211_P902211_SBA_BANK_CODE_dlg > div.a-PopupLOV-searchBar > input',
    BANK_LIST : '#PopupLov_902211_P902211_SBA_BANK_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul',
    SCHEME_BTN :'#P902211_SBA_SYS_CODE_lov_btn',
    SCHEME_SEARCH_INP : '#PopupLov_902211_P902211_SBA_SYS_CODE_dlg > div.a-PopupLOV-searchBar > input',
    SCHEME_LIST :'#PopupLov_902211_P902211_SBA_SYS_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul',
    MEM_SCHI_TYPE :'#P902211_SBA_MEMB_SCHI_TYPE',
    PRINC_BANK : '#P902211_SBA_PRIN_BAN_CODE',
    BIN_RFC: '#P902211_SBA_RFC_BIN',
    
}
/// <reference types='cypress'/>
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
describe('Bank Card Generation Method.', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        // Step 01 :  Access
        cy.viewport(1920, 1080)
        cy.get(MENU.SETTINGS).click({force:true})
        cy.get(NEW_CENTER.BASE_PARAM).click();
    });
    
    it('Delete Bank Card Generation Method -Nominal case.',()=>{
        cy.get('#\\35 22408934932516876 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('.is-selected > :nth-child(2) > a > .fa').click();
        cy.handleModal()
        cy.getIframeDom('#\\32 35028513001019165 > li:nth-child(6) > a').click()
        cy.get('.a-GV-row.is-selected > .has-button > .a-Button > .a-Icon').click()
        cy.get('#R301831544123319423_ig_row_actions_menu_2i').click()
        cy.get('.a-Button--hot').should('be.visible').click()
    })
})
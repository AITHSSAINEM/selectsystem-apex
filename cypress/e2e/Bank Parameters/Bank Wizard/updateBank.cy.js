/// <reference types ="cypress" />
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import { ACCESS, BANK_DATA, BANK_CURRENCY, PARAMETER ,DEFAULT_ACCOUNT , SUMMARY} from "../../../loctors/bank/bank";
import { ERROR } from "../../../loctors/bank/errors";
describe('Create BANK', ()=>{
    
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        cy.viewport(1920, 1080)
        // Step 01 :  Access
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(ACCESS.BANK_MANAGEMENT_ITEM).click()
    
    })

    it('Test update bank', ()=>{

      cy.get('#myreg_ig_grid_vc_cur > a > .apex-edit-pencil-alt').click();
      cy.get('a[aria-controls="R229880749025775419"]').click({force: true})

      cy.get('#P902203_BAN_IDEN_CONTAINER > .t-Form-inputContainer').click();
      cy.get('#P902203_BAN_IDEN').clear('10006');
      cy.get('#P902203_BAN_IDEN').type('10006');
      cy.get('#P902203_BAN_CORP_NAME').clear('BICICI');
      cy.get('#P902203_BAN_CORP_NAME').type('BICICI');
      cy.get('#P902203_BAN_INIT').clear('BICICI');
      cy.get('#P902203_BAN_INIT').type('BICICI');
      cy.get('#R229881708602775429_tab > a > span').click();
      cy.get(':nth-child(1) > .u-radio').click();
      cy.get('#P902203_BAN_ORIG_CURR_RATE_0').check();
      cy.get(':nth-child(2) > .u-radio').click();
      cy.get('#P902203_BAN_ORIG_CURR_RATE_1').check();
      cy.get('#R229880866326775420_tab > a > span').click();
      cy.get('#P902203_BAN_ISSU_ACQU_COM_CODE').select('302');
      cy.get('#P902203_BAN_ISSU_COM_CODE').select('841');
      cy.get('#P902203_BAN_ACQU_COM_CODE').select('6223');
      cy.get('#R229880973530775421_tab > a > span').click();
      cy.get('#R229881109427775423_tab > a > span').click();
      cy.get('#myregsac_tab > a > span').click();
      cy.get('#R229881398013775425_tab > a > span').click();
      cy.get('#R870624447908623106_tab > a > span').click();
      cy.get('#R502303951894942816_tab > a > span').click();
      cy.get('#B231837788870467778 > .t-Button-label').should('be.visible');
      cy.get('#B231837788870467778 > .t-Button-label').should('have.text', 'Apply Changes');
    })
})
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
import { LOGIN_PAGE } from '../loctors/login'
import loginData from '../fixtures/login.json'
import baseUrl from '../variables/baseUrl.json'

Cypress.Commands.add('login', () => {
    cy.visit(baseUrl.baseUrl , {
      chromeWebSecurity: false,
      incognito: true
    })
    cy.get(LOGIN_PAGE.USERNAME_FIELD).type(loginData.username)
    cy.get(LOGIN_PAGE.PASSWORD_FIELD).type(loginData.password).clear()
    cy.get(LOGIN_PAGE.LOGIN_BUTTON).click()
    // Vérifiez que l'utilisateur est connecté avec succès
    cy.title().should('include', 'Home')
    cy.viewport(1920, 1080)
 })

 Cypress.Commands.add('login2', (username, password) => {
  cy.visit(baseUrl.baseUrl , {
    chromeWebSecurity: false,
    incognito: true
  })
  cy.get(LOGIN_PAGE.USERNAME_FIELD).type(username)
  cy.get(LOGIN_PAGE.PASSWORD_FIELD).type(password)
  cy.get(LOGIN_PAGE.LOGIN_BUTTON).click()
  // Vérifiez que l'utilisateur est connecté avec succès
  //cy.title().should('include', 'Home')
})
 
Cypress.Commands.add('getIframeDom', (selectorElement)=>{
    cy.get('iframe').invoke('contents')
        .invoke('find', 'body')
        .then(cy.wrap)
        .find(selectorElement)
})

Cypress.Commands.add('handleModal', ()=>{
    cy.get('iframe').then(frame =>{
        return new Cypress.Promise((resolve) => {
          frame.on('load', () => {
            resolve(frame.contents().find('body'))
          })
        })
      })
})
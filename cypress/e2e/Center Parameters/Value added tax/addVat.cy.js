/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Value Added Tax (VAT)',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get('#\\35 22400360937447804 > li:nth-child(3) > a').click()
        
    })

    it('Add vat. - Nominal case ', ()=>{
        cy.get('#mybtn1').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height', '300px')
        cy.getIframeDom('#P900111_VAT_IDEN').type('012')
        cy.getIframeDom('#P900111_VAT_LABE').type('TVA BICICI')
        cy.getIframeDom('#B238046505150157573').should('be.visible').click()
        cy.get('#t_Alert_Success').should('be.visible')

    })
    it('Add vat- mandatory field lef empty - Exptional case ', ()=>{
        cy.get('#mybtn1').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height', '300px')
        cy.getIframeDom('#P900111_VAT_IDEN').clear()
        cy.getIframeDom('#P900111_VAT_LABE').type('TVA BICICI')
        cy.getIframeDom('#B238046505150157573').should('be.visible').click()
        cy.getIframeDom('#P900111_VAT_IDEN_error').should('be.visible').and('have.text','Iden must have some value.')
        cy.getIframeDom('#t_Alert_Notification').should('be.visible')
    //cy.get('#t_Alert_Success').should('be.visible')
    })
})
/// <reference types="cypress"/>
import {MENU} from '../../../../loctors/menu'
import { NEW_CENTER , ADDRESS , UI_DIALOG, FIRS_CONTACT, SEC_CONTACT, CORRESPENDANCE } from '../../../../loctors/center'
describe('Center : Interchange commission',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
    })

    it(' Update interchange Commission   - Nominal case ', ()=>{
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(3) > a').click()
        cy.get('[data-id="7"] > :nth-child(1) > a > .apex-edit-pencil-alt').click()
        cy.get('#P100302_COM_IDEN').clear().type('0005');
        cy.get('#P100302_COM_RATE').clear().type('2');
        cy.get('#P100302_COM_MIN').clear().type('12');
        cy.get('#P100302_COM_LABE').clear().type('Interchange Commission');
        cy.get('#P100302_COM_FIXE').clear().type('14');
        cy.get('#P100302_COM_MAX').clear().type('20');
        cy.get('#B234441970728367453').should('be.visible').click()
    })
})
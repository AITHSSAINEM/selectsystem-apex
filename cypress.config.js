const { defineConfig } = require("cypress");
const allureWriter = require('@shelex/cypress-allure-plugin/writer')

module.exports = {
  chromeWebSecurity: false, // set to false to disable web security
  chromeWebSecurity: {
    incognito: true // set to true to open browser in incognito mode
  }
}




module.exports = defineConfig({
  projectId: 'u6i1ow',
  record : true,
  parallel: true,
  key : "dbfff49c-71e3-4f41-8413-c6bdeb368ce1",
  e2e: {
    experimentalStudio: true,
    setupNodeEvents(on, config) {
      allureWriter(on, config);
    
    },
  },
});

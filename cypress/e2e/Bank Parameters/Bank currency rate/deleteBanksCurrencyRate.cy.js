/// <reference types='cypress'/>
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import {SCHEME} from "../../../loctors/bank payment scheme/bankPaymentScheme"
describe('Bank currency rate - nominal case ', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        // Step 01 :  Access
        cy.viewport(1920, 1080)
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    });
    

    it('delete bank\'s currency rate. -Nominal case.', ()=>{
        cy.get('#\\35 22408934932516876 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('.is-selected > :nth-child(2) > a > .fa').click();
        cy.handleModal()
        cy.getIframeDom('#\\32 35028513001019165 > li:nth-child(2) > a > span.t-LinksList-label').click()

        cy.get('#R598318356113698555_ig_grid_vc_cur > a').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','500px') 
        cy.getIframeDom('#B364421271840536654').should('be.visible').click()
        cy.get('.js-confirmBtn').click()
    })
})
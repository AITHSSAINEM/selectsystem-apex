export const ERROR = {
    ID: "#P902206_BAN_IDEN_error",
    COUNTRY : "#P902206_BAN_ACQU_COUN_CODE_error",
    CORP_NAME : "#P902206_BAN_CORP_NAME_error",
    TYPE : "#P902206_BAN_BANK_TYPE_CODE_error",
    ALERT : "#t_Alert_Notification > div"
    }
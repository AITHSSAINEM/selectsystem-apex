/// <reference types ="cypress" />
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import { ACCESS} from "../../../loctors/bank/bank";
import { Commission, Revolving, Settlement} from "../../../loctors/bank/configure";
describe('Create BANK', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        // Step 01 :  Access
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(ACCESS.BANK_MANAGEMENT_ITEM).click()
        cy.get(Commission.PENCIL).click();
    })

    it('Configure Bank\'s Revolving Date - Nominal Case', function() {
        cy.get(Revolving.LINK).click();
        cy.get(Revolving.LAST_BUS_DATE_BTN).click();
        cy.get(Revolving.LAST_BUS_DATE_YEAR).select('2022');
        cy.get(Revolving.LAST_BUS_DATE_MONTH).select('9');
        cy.get(Revolving.LAST_BUS_DATE_DAY).click();
        cy.get('#P902203_BAN_LAST_BUSI_DATE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper').click();
        cy.get(Revolving.BUS_DATE_BTN).click();
        cy.get(Revolving.BUS_DATE_YEAR).select('2022');
        cy.get(Revolving.BUS_DATE_MONTH).select('10');
        cy.get(Revolving.BUS_DATE_DAY).click();
        cy.get('#main').click();
        cy.get(Revolving.APPLY_CHANGES).should('be.visible');

        // Pour appliquer les modifications ci-dissus, il suffit d'activer la ligne suivante : 
       //cy.get(Revolving.APPLY_CHANGES).click()
        
    });


    
})
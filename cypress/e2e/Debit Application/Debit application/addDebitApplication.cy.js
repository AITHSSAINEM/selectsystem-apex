/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import {DEBIT_APP, NEW_APP ,CUSTOMER_INFO, ADDRESS, RISK, Fees, CARDHOLDER, SUMMARY} from '../../../loctors/debitApplication'
import debitApp  from '../../../fixtures/Issuing/Application Management/DebitApplication.json'
describe('Debit Management ',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.ISSUING).click()
    })
    it('Add Debit Application - Nominal Case', ()=>{
        // affiche la la boite de dialoge pour creér une nouvelle debit application 
       
        cy.get(DEBIT_APP.APP_MANAGEMENT_SECTION).should('be.visible')
        cy.get(DEBIT_APP.DEBIT_APP_BUTTON).contains('Debit Application').click()
        cy.get(NEW_APP.NEW_APP_BUTTON).contains('New Application').click()
        cy.get(NEW_APP.UI_DIALOG).should('be.visible')
        cy.get(NEW_APP.UI_DIALOG).invoke('css', 'height', '800px')
        
        // cette methode sert à attendre le chargement des DOMs de iframe (att dynamique)
       
        cy.handleModal()

        // Step 1 : Define Customer Identity: 

        // getIframeDom c'est une nouvelle Commande qui nous permet de trouver les Doms de l'iframe 
       
        cy.getIframeDom(NEW_APP.BANK_FIELD).select(debitApp.CUSTOMER_IDENT.BANK.VALUE).contains(debitApp.CUSTOMER_IDENT.BANK.NAME)
        cy.getIframeDom(NEW_APP.BRANCH_FIELD).select(debitApp.CUSTOMER_IDENT.BRANCH.VALUE)
        cy.getIframeDom(NEW_APP.NEW_CUSTOMER).click()
        cy.getIframeDom(NEW_APP.IDEN_FIELD).type(debitApp.CUSTOMER_IDENT.IDEN)
        cy.getIframeDom(NEW_APP.VIP_CATEGORY).select(debitApp.CUSTOMER_IDENT.VIP_CAT.VALUE).contains(debitApp.CUSTOMER_IDENT.VIP_CAT.TITLE) 
        cy.getIframeDom(NEW_APP.PARENT_BTN).click()
        cy.get(NEW_APP.PARENT).type('mohamed{enter}')
        cy.get(NEW_APP.FIRST_PARENT).click()
        cy.getIframeDom(NEW_APP.NEXT_BUTTON).click()

       //Step 2: Fill In Customer Information Form
        
       cy.handleModal()
       cy.getIframeDom(CUSTOMER_INFO.CUSTOMER_TITLE).select(debitApp.CUSTOMER_INFO.CUSTOMER_TITLE.VALUE).contains(debitApp.CUSTOMER_INFO.CUSTOMER_TITLE.TITLE)
       cy.getIframeDom(CUSTOMER_INFO.CUSTOMER_NAME).type(debitApp.CUSTOMER_INFO.FIRST_NAME)
       cy.getIframeDom(CUSTOMER_INFO.CUSTOMER_MIDDEL).type(debitApp.CUSTOMER_INFO.MIDDEL_NAME)
       cy.getIframeDom(CUSTOMER_INFO.CUSTOMER_LST_NAME).type(debitApp.CUSTOMER_INFO.LAST_NAME)
       cy.getIframeDom(CUSTOMER_INFO.CUSTOMER_IDEN_1).select(debitApp.CUSTOMER_INFO.FIRST_IDENTITY.INDEX).contains(debitApp.CUSTOMER_INFO.FIRST_IDENTITY.TITLE)
       cy.getIframeDom(CUSTOMER_INFO.CUSTOMER_IDEN_1_VAL).type(debitApp.CUSTOMER_INFO.FIRST_IDENTITY.VALUE)
       cy.getIframeDom(CUSTOMER_INFO.CUSTOMER_IDEN_2).select(debitApp.CUSTOMER_INFO.SECOND_IDENTITY.INDEX).contains(debitApp.CUSTOMER_INFO.SECOND_IDENTITY.TITLE)
       cy.getIframeDom(CUSTOMER_INFO.CUSTOMER_IDEN_2_VAL).type(debitApp.CUSTOMER_INFO.SECOND_IDENTITY.VALUE)
       cy.getIframeDom(CUSTOMER_INFO.NATIONALITY_BTN).click()
       cy.get(CUSTOMER_INFO.CUSTOMER_NATIONALITY).type('Ivory Coast{enter}')
       cy.get(CUSTOMER_INFO.FIRST_ELE).first().click()
       cy.getIframeDom(CUSTOMER_INFO.CUSTOMER_DEPE_NUMBER).type(debitApp.CUSTOMER_INFO.DEPENDENTS_NUMBER)
       cy.getIframeDom(CUSTOMER_INFO.CUSTOMER_GENRE).select(debitApp.CUSTOMER_INFO.GENRE.INDEX).contains(debitApp.CUSTOMER_INFO.GENRE.TITLE)
       cy.getIframeDom(CUSTOMER_INFO.CUSTOMER_STATUS).select(debitApp.CUSTOMER_INFO.STATUS.INDEX).contains(debitApp.CUSTOMER_INFO.STATUS.TITLE)
       cy.getIframeDom(CUSTOMER_INFO.CUSTOMER_BIRTHDAY).type(debitApp.CUSTOMER_INFO.BIRTHDAY)
       cy.getIframeDom(CUSTOMER_INFO.CUSTOMER_BIR_LOCATION).type(debitApp.CUSTOMER_INFO.BIRTHDAY_LOCATION)
       cy.getIframeDom(CUSTOMER_INFO.NEXT_BUTTON).click()

       //Step 3 : Add Customer Address & Optional Contacts

        cy.handleModal()
        cy.getIframeDom(ADDRESS.ADDRESS_TYPE).select(debitApp.ADDRESS.TYPE.INDEX)
        cy.getIframeDom(ADDRESS.ADDRESS_LIGNE1).type(debitApp.ADDRESS.LIGNE1)
        cy.getIframeDom(ADDRESS.ADDRESS_LIGNE2).type(debitApp.ADDRESS.LIGNE2)
        cy.getIframeDom(ADDRESS.STREET).type(debitApp.ADDRESS.STREET)
        cy.getIframeDom(ADDRESS.PO_BOX).type(debitApp.ADDRESS.PO_BOX)
        cy.getIframeDom(ADDRESS.ZIP).type(debitApp.ADDRESS.ZIP)
        cy.getIframeDom(ADDRESS.STATE).type(debitApp.ADDRESS.STATE)
        cy.getIframeDom(ADDRESS.CITY).select(debitApp.ADDRESS.CITY.INDEX)
        cy.getIframeDom(ADDRESS.PHONE1).type(debitApp.ADDRESS.PHONE1)
        cy.getIframeDom(ADDRESS.PHONE2).type(debitApp.ADDRESS.PHONE2)
        cy.getIframeDom(ADDRESS.FAX).type(debitApp.ADDRESS.FAX)
        cy.getIframeDom(ADDRESS.NEXT_BUTTON).click()

       // Step 4: Set Risk & Routed Account

       cy.handleModal()
       cy.getIframeDom(RISK.CUSTOMER_RISK).select(debitApp.RISK.CUSTOMER_RISK.INDEX)
       cy.getIframeDom(RISK.HOST).type(debitApp.RISK.HOST)
       cy.getIframeDom(RISK.BANK_ACCOUNT_TYPE).select(debitApp.RISK.BANK_ACCOUNT_TYPE.INDEX)
       cy.getIframeDom(RISK.CURRENCY).select(debitApp.RISK.CURRENCY.INDEX)
       cy.getIframeDom(RISK.ACCOUNT_PROGRAM).select(debitApp.RISK.ACCOUNT_PROGRAM.INDEX)
       cy.getIframeDom(RISK.NEXT_BUTTON).click()
        
       // Step 5: Choose Program & Fees
       
       cy.handleModal()
       cy.getIframeDom(Fees.CARD_PRODUCT).select('931')

       cy.getIframeDom(Fees.CARD_NUMBER)

       .should('exist')
       .should('be.visible')
       .should('have.attr', 'type', 'text')
       .should('not.be.disabled')

       cy.getIframeDom(Fees.EFFE_DATE)
       .should('exist')
       .should('be.visible')
       .should('have.attr', 'type', 'text')
       .should('not.be.disabled')

       cy.getIframeDom(Fees.EXPIRY_DATE)
       .should('exist')
       .should('be.visible')
       .should('have.attr', 'type', 'text')
       .should('not.be.disabled')
        
       //cy.getIframeDom(Fees.CARD_LEVEL).click()

       cy.getIframeDom(Fees.PERS_FEE).select('292')
       cy.getIframeDom(Fees.PRES_IDEN)
       .should('exist')
       .should('be.visible')
       .should('have.attr', 'type', 'text')
       .should('not.be.disabled')
       
       /*
       cy.getIframeDom(Fees.AMOUNT_FEE)
       .should('exist')
       .should('be.visible')
       .should('have.attr', 'type', 'text')
       .should('not.be.disabled')
       */
       cy.getIframeDom(Fees.MEM_FEE_LINK).click()
       cy.getIframeDom(Fees.MEM_FEE_SELE).select('533')
       cy.getIframeDom(Fees.MEM_IDEN)
       .should('exist')
       .should('be.visible')
       .should('have.attr', 'type', 'text')
       .should('not.be.disabled')
        
       cy.getIframeDom(Fees.DESIGN_FEE_LINK).click()
       cy.getIframeDom(Fees.NEW_DESIGN).then(($checkout) => {
        if (!$checkout.is(':checked')) {
          cy.getIframeDom(Fees.NEW_DESIGN).check({force: true})
        }
      });
       cy.getIframeDom(Fees.DESIGN_FEE_SELE).select('1')
       
       cy.getIframeDom(Fees.DELL_FEE_LINK).click()
       /cy.getIframeDom(Fees.EXPRESS_DELLVERY).then(($checkout) => {
        if (!$checkout.is(':checked')) {
          cy.getIframeDom(Fees.EXPRESS_DELLVERY).click({force: true});
        }
      });
      cy.getIframeDom(Fees.DELL_OPERATION).select('1')
      cy.getIframeDom(Fees.NUMB_CARDS).type('2')
      cy.getIframeDom(Fees.DELL_FEE_SELECT).select('1')
      
      cy.getIframeDom(Fees.NEXT_BUTTON).click()

      //Step 6: Complete Cardholder Information
       cy.handleModal()
       cy.getIframeDom(CARDHOLDER.TITLE).select('1')
       cy.getIframeDom(CARDHOLDER.FIRST_NAME).clear().type('MUSTAPHA')
       cy.getIframeDom(CARDHOLDER.MIDDLE_NAME).clear().type('A.')
       cy.getIframeDom(CARDHOLDER.LAST_NAME).clear().type('Ahmed')
       cy.getIframeDom(CARDHOLDER.CORPORATE_NAME).clear().type('ANR inc.')
       cy.getIframeDom(CARDHOLDER.CARDHOLDER_NAME).clear().type('Mustapha A. Ahmed')
       cy.getIframeDom(CARDHOLDER.PHONE_NUMBER).type('0654213254')
       cy.getIframeDom(CARDHOLDER.EMAIL).type('m.ahmed@gmail.ic')
       cy.getIframeDom(CARDHOLDER.NEXT_BUTTON).click()
       

       // SUMMAMRY 
       cy.handleModal()
       cy.getIframeDom(SUMMARY.CUSTOMMER_IDEN_SEC).should('be.visible')
    // cy.getIframeDom(SUMMARY.CUSTOMMER_INF_SEC).should('be.visible')
       cy.getIframeDom(SUMMARY.CREATE_APP_BTN).should('be.visible')
    })

     
})
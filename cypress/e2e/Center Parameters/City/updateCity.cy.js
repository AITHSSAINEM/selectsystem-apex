/// <reference types ="cypress"/>
import {MENU} from '../../../loctors/menu'
import {CITY} from '../../../loctors/city'
import { NEW_CENTER}from '../../../loctors/center'
describe('CITY ', ()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
    })

    it('Update city.',()=>{
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get('#\\35 23404071394758395 > :nth-child(2) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get(CITY.PENCIL_ICON).click();
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height', '310px')
        cy.handleModal()
        cy.getIframeDom(CITY.IDEN).clear().type('Casablanca')
        cy.getIframeDom(CITY.NAME).clear().type('Casablanca')
        cy.getIframeDom(CITY.SERACH_BTN).click()
        cy.get(CITY.SERACH_INP).type('Morocco{enter}')
        cy.get(CITY.COUNTRY).first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get(CITY.COUNTRY).first().children().first().click()
            }
          })


        cy.getIframeDom(CITY.CANCEL_BTN).should('be.visible')
        cy.getIframeDom(CITY.APPLY_CHANGES).should('be.visible').click()
        cy.get('#t_Alert_Success').should('be.visible')
        
    })

})
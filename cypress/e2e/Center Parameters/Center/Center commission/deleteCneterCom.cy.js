/// <reference types="cypress"/>
import {MENU} from '../../../../loctors/menu'
import { NEW_CENTER , ADDRESS , UI_DIALOG, FIRS_CONTACT, SEC_CONTACT, CORRESPENDANCE } from '../../../../loctors/center'
describe('Center Commission ',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#myreg_data_tab > a > span').should('be.visible').and('have.text','Center Menu').click()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(2) > a').should('be.visible').contains('Center Commission').click()
    })

    it(' delete center commission  - Nominal case ', ()=>{
       
        cy.get('#myreg_ig_grid_vc_cur > a').click();
        cy.get('#P100302_COM_RATE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper').click();
        cy.get('#B234441594671367453').should('be.visible');
        cy.get('#B234441594671367453 > .t-Button-label').should('have.text', 'Delete');
        

    // To delete a center commission, all that needs to be done is to activate the following line : 
       //cy.get('#B234441594671367453').click();
        
    })
})
/// <reference types ="cypress" />
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import { ACCESS} from "../../../loctors/bank/bank";
import { Commission, Revolving, Settlement} from "../../../loctors/bank/configure";
import {  UI_DIALOG, FIRS_CONTACT, SEC_CONTACT } from '../../../loctors/center'

describe('Create BANK', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()

        // Step 01 :  Access
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(ACCESS.BANK_MANAGEMENT_ITEM).click()
        cy.get(Commission.PENCIL).click();
    })
    it('Configure Bank\'s Center Commission - Nominal Case', ()=>{
        cy.get(Commission.LINK).click();
        cy.get(Commission.BOTH).select('302');
        cy.get(Commission.ISSUER).select('302');
        cy.get(Commission.ACQUIRER).select('302');
        cy.get(Commission.APPLY_CHANGES).should('be.visible');

        // Pour sauvegarder les modifications, il suffit d'activer la lige suivante 
       // cy.get(Commission.APPLY_CHANGES).click()
    })


})
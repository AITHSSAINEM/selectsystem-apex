import {MENU} from '../../../../loctors/menu'
import { NEW_CENTER} from '../../../../loctors/center'
describe('Center : Interchange commission',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
    })

    it(' delete interchange Commission  record- Nominal case ', ()=>{
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(3) > a').click()
        cy.get('#myreg_ig_grid_vc_cur > a').click()
        cy.get('#myreg_ig_grid_vc_cur > a').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height', '600px')
        cy.getIframeDom('#B837611470694927275').should('be.visible').and('have.text', 'Delete').click()
        cy.get('#t_PageBody > div.ui-dialog.ui-dialog--notification.ui-dialog--modern.ui-dialog--danger.ui-widget.ui-widget-content.ui-front.ui-dialog-buttons.ui-draggable')
        .should('be.visible')
        cy.get('#t_PageBody > div.ui-dialog.ui-dialog--notification.ui-dialog--modern.ui-dialog--danger.ui-widget.ui-widget-content.ui-front.ui-dialog-buttons.ui-draggable > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button.js-confirmBtn.ui-button.ui-corner-all.ui-widget.ui-button--danger')
        .should('be.visible').and('have.text', 'Delete').click()

    })
})
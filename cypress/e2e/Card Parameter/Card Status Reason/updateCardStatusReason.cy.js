
/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
describe('Card Status Reason.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Update Card Status Reason -Nominal case ',()=>{
        
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#\\36 66871638801445837 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#myreg_ig_grid_vc_cur > a').click()
        
  
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
         .invoke('css','height', '350px')
        cy.handleModal()

        
        cy.getIframeDom('#P101023_CSR_BAN_CODE_lov_btn').click()
        cy.get('#PopupLov_101023_P101023_CSR_BAN_CODE_dlg > div.a-PopupLOV-searchBar > input')
        .type('BICICI{enter}')
        cy.get('#PopupLov_101023_P101023_CSR_BAN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste').first().children().first().click()
            }
          })



        cy.getIframeDom('#P101023_CSR_STAT_CODE_lov_btn').click()
        cy.get('#PopupLov_101023_P101023_CSR_STAT_CODE_dlg > div.a-PopupLOV-searchBar > input')
        .type('Active{enter}')

        cy.get('#PopupLov_101023_P101023_CSR_STAT_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste2').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste2').first().children().first().click()
            }
          })

        cy.getIframeDom('#P101023_CSR_IDEN').clear().type('008')
        cy.getIframeDom('#P101023_CSR_LABE').clear().type('Bank Decision')
        
        cy.getIframeDom('#B229774144347692471').should('be.visible').click()
        cy.get('#t_Alert_Success').should('be.visible')
       
       

    })
})
/// <reference types='cypress'/>
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import {BRANCH} from "../../../loctors/bank/branch"
import {UI_DIALOG, FIRS_CONTACT, SEC_CONTACT} from '../../../loctors/center'
describe('branch- nominal case ', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        cy.viewport(1920, 1080)
        // Step 01 :  Access
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    });

    it('Test add new branch',()=>{
       
        cy.get('#\\35 22408934932516876 > :nth-child(2) > .t-LinksList-link').click();
        cy.get('.t-Button-label').click();
        cy.get(BRANCH.IDEN).type('11542');
        cy.get(BRANCH.NAME).type('BICICI North');
        cy.get(BRANCH.INIT).type('BICICIN');
        cy.get(BRANCH.BANK).select('20');
        cy.get(BRANCH.CREATE_BTN).should('be.visible');

        // pour creer un une nouvelle branche, il suffit d'activer la ligne suivante : 
        cy.get('#B238278738036431769').click()
        
    })

})
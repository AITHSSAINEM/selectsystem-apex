/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import {DEBIT_APP, NEW_APP} from '../../../loctors/debitApplication'
import {ERROR_MESSAGE } from '../../../loctors/errors/debitAppErrors'
describe('Add Debit Application - Required Fields Empty - ',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.ISSUING).click()
    })
    it(' Test Add Debit Application - Required Fields Empty -', ()=>{
        // affiche la la boite de dialoge pour creér une nouvelle debit application 
       
        cy.get(DEBIT_APP.APP_MANAGEMENT_SECTION).should('be.visible')
        cy.get(DEBIT_APP.DEBIT_APP_BUTTON).contains('Debit Application').click()
        cy.get(NEW_APP.NEW_APP_BUTTON).contains('New Application').click()
        cy.get(NEW_APP.UI_DIALOG).should('be.visible')
        cy.get(NEW_APP.UI_DIALOG).invoke('css', 'height', '800px')
        
        // cette methode sert à attendre le chargement des DOMs de iframe (att dynamique)
       
        cy.handleModal()

        // Step 1 : Define Customer Identity: 

        // getIframeDom c'est une nouvelle Commande qui nous permet de trouver les Doms de l'iframe 
        cy.getIframeDom(NEW_APP.NEXT_BUTTON).click()

        // l'assertion des erreurs.
        cy.getIframeDom(ERROR_MESSAGE.BANK_ER).should('be.visible').contains("Bank must have some value.")
        cy.getIframeDom(ERROR_MESSAGE.BRANCH_ER).should('be.visible').contains('Branch must have some value.')
        cy.getIframeDom(ERROR_MESSAGE.VIP_CAT_Er).should('be.visible').contains('VIP Category must have some value.')
        cy.getIframeDom(ERROR_MESSAGE.ALERT_ER).should('be.visible')
    })
})
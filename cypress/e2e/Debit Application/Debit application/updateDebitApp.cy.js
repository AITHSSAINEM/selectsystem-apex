/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import {DEBIT_APP, NEW_APP} from '../../../loctors/debitApplication'
import {ERROR_MESSAGE } from '../../../loctors/errors/debitAppErrors'
describe('Debit Application.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.ISSUING).click()
    })
    it('Updte Debit Application', ()=>{
        // affiche la la boite de dialoge pour creér une nouvelle debit application 

        cy.get(DEBIT_APP.APP_MANAGEMENT_SECTION).should('be.visible')
        cy.get(DEBIT_APP.DEBIT_APP_BUTTON).contains('Debit Application').click()
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#debits_id2_ig_grid_vc_cur > a > .apex-edit-pencil-alt').click();
        cy.get('#R299165517905478907 > :nth-child(2) > :nth-child(2) > :nth-child(3) > :nth-child(1) > .col-12').should('be.visible');
        cy.get('#R299165517905478907 > :nth-child(2) > :nth-child(2) > :nth-child(3) > :nth-child(2) > :nth-child(1)').should('be.visible');
        cy.get('#R299165517905478907 > :nth-child(2) > :nth-child(2) > :nth-child(3) > :nth-child(3) > :nth-child(1)').should('be.visible');
        cy.get('#R299165517905478907 > :nth-child(2) > :nth-child(2) > :nth-child(3) > :nth-child(5) > :nth-child(1)').should('be.visible');
        cy.get('#R299165517905478907 > :nth-child(2) > :nth-child(2) > :nth-child(3) > :nth-child(6) > :nth-child(1)').should('be.visible');
        /* ==== End Cypress Studio ==== */
    })
    
})
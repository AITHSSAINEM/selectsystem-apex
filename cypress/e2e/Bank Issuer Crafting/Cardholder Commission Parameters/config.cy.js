
/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Cardholder Commission Parameters .',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    })

    it('Configure Cardholder Commission Parameters . - Nominal case.', ()=>{
        
        cy.get('#\\35 23400178171740781 > :nth-child(2) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#R182223024764852463_ig_grid_vc_cur > a > :nth-child(2)').click();
        cy.get('b').click();
        cy.get('#R163518096416426447_tab > a > span').click();
        cy.get('#P900211_CTY_MCC_GROU').clear('1');
        cy.get('#P900211_CTY_MCC_GROU').type('1');
        cy.get('#P900211_CTY_MAIL_ORDE_INTE_ORDE').clear();
        cy.get('#P900211_CTY_MAIL_ORDE_INTE_ORDE').type('2');
        cy.get('#P900211_CTY_CARD_USAG').clear();
        cy.get('#P900211_CTY_CARD_USAG').type('1');
        cy.get('#P900211_CTY_TRAN_CODE').clear();
        cy.get('#P900211_CTY_TRAN_CODE').type('5');
        cy.get('#P900211_CTY_PCA_CODE').clear();
        cy.get('#P900211_CTY_PCA_CODE').type('7');
        cy.get('#P900211_CTY_COUN_GROU').clear();
        cy.get('#P900211_CTY_COUN_GROU').type('1');
        cy.get('#B128787068630191877').should('be.visible');

        // cy.get('#B128787068630191877').click()
        
    })
})
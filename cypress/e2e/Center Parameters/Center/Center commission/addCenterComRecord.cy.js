/// <reference types="cypress"/>
import {MENU} from '../../../../loctors/menu'
import { NEW_CENTER , ADDRESS , UI_DIALOG, FIRS_CONTACT, SEC_CONTACT, CORRESPENDANCE } from '../../../../loctors/center'
describe('Center Commission ',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#myreg_data_tab > a > span').should('be.visible').and('have.text','Center Menu').click()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(2) > a').should('be.visible').contains('Center Commission').click()
    })

    it(' Add center commission record  - Nominal case ', ()=>{
       
        cy.get('#myreg_ig_grid_vc_cur > a').click();
        cy.get('#P100302_COM_RATE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper').click();
        cy.get('#mybtn').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height', '500px')
        cy.getIframeDom('#P100305_CRE_DEBI_CRED').select('D')
        cy.getIframeDom('#P100305_CRE_TRAN_CODE').select('07000')
        cy.getIframeDom('#P100305_CRE_PCA_CODE').select('1')
        cy.getIframeDom('#P100305_CRE_CCG_CODE').select('1')
        cy.getIframeDom('#P100305_CRE_CAU_CODE').select('C')
        cy.getIframeDom('#P100305_CRE_MAG_CODE').select('262')
        cy.getIframeDom('#P100305_CRE_MAIL_ORDE_INTE_ORDE').select('C')
        cy.getIframeDom('#P100305_CRE_COMM_FROM').type('transaction')
        cy.getIframeDom('#P100305_CRE_RATE').type('2')
        cy.getIframeDom('#P100305_CRE_FIXE').type('12')
        cy.getIframeDom('#P100305_CRE_MIN').type('10')
        cy.getIframeDom('#P100305_CRE_MAX').type('30')

        cy.getIframeDom('#B837611083688927275').should('be.visible').and('have.text', 'Create')
        
        cy.getIframeDom('#B837611083688927275').click()


    
    })
})
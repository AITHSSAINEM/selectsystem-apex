export const MENU = {
    ISSUING   : '#t_MenuNav_0i',
    ACQUIRING : '#t_MenuNav_1i',
    SWITCH    : '#t_MenuNav_2i',
    SCHEMES   : '#t_MenuNav_3i',
    DASHBOARD : '#t_MenuNav_4i',
    REPORTING : '#t_MenuNav_5i',
    SETTINGS  : '#t_MenuNav_6i'
}
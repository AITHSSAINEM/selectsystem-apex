import {MENU} from '../../../loctors/menu'
describe('Automatic Renewal To Chip.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Add Automatic Renewal To Chip -Nominal case ',()=>{
      
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link').click();
        cy.get('#\\36 66871638801445837 > :nth-child(7) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#myreg_ig_grid_vc_cur > a').click()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height', '450px')
        cy.handleModal()

        cy.getIframeDom('#P101033_RAB_BAN_CODE_lov_btn').click()
        cy.get('#PopupLov_101033_P101033_RAB_BAN_CODE_dlg > div.a-PopupLOV-searchBar > input')
        .type('BICICI{enter}')
        cy.get('#PopupLov_101033_P101033_RAB_BAN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste1').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste1').first().children().first().click()
            }
          })

          /********************************************************/

          cy.getIframeDom('#P101033_RAB_CPR_CODE_MAG_lov_btn').click()
          cy.get('#PopupLov_101033_P101033_RAB_CPR_CODE_MAG_dlg > div.a-PopupLOV-searchBar > input')
          .type('VISA Classic{enter}')
          cy.get('#PopupLov_101033_P101033_RAB_CPR_CODE_MAG_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste2').first().then(($ul) => {
              // Vérifiez si l'élément ul existe
              if ($ul.length) {
                // Sélectionnez le premier enfant de ul
                cy.get('@liste2').first().children().first().click()
              }
            })

            /**************************************** */
            cy.getIframeDom('#P101033_RAB_CPR_CODE_lov_btn').click()
            cy.get('#PopupLov_101033_P101033_RAB_CPR_CODE_dlg > div.a-PopupLOV-searchBar > input')
            .type('BICICI{enter}')
            cy.get('#PopupLov_101033_P101033_RAB_CPR_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste3').first().then(($ul) => {
                // Vérifiez si l'élément ul existe
                if ($ul.length) {
                  // Sélectionnez le premier enfant de ul
                  cy.get('@liste3').first().children().first().click()
                }
              })
                 /**************************************** */
            cy.getIframeDom('#P101033_RAB_PFE_CODE_lov_btn').click()
            cy.get('#PopupLov_101033_P101033_RAB_PFE_CODE_dlg > div.a-PopupLOV-searchBar > input')
            .type('visa{enter}')
            cy.get('#PopupLov_101033_P101033_RAB_PFE_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste4').first().then(($ul) => {
                // Vérifiez si l'élément ul existe
                if ($ul.length) {
                  // Sélectionnez le premier enfant de ul
                  cy.get('@liste4').first().children().first().click()
                }
              })
                 /**************************************** */
            cy.getIframeDom('#P101033_RAB_CMF_CODE_lov_btn').click()
            cy.get('#PopupLov_101033_P101033_RAB_CMF_CODE_dlg > div.a-PopupLOV-searchBar > input')
            .type('class{enter}')
            cy.get('#PopupLov_101033_P101033_RAB_CMF_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste5').first().then(($ul) => {
                // Vérifiez si l'élément ul existe
                if ($ul.length) {
                  // Sélectionnez le premier enfant de ul
                  cy.get('@liste5').first().children().first().click()
                }
              })


              cy.getIframeDom('#DELIVRY_EXPRESS').then($checkbox => {
                if ($checkbox.is(':checked')) {
                  cy.wrap($checkbox).uncheck({force:true});
                } else {
                  cy.wrap($checkbox).check({force:true});
                }
              });
          
        cy.getIframeDom('#B233731025365834197').should('be.visible').click()
        
    })
})
/// <reference types='cypress'/>
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import {SCHEME} from "../../../loctors/bank payment scheme/bankPaymentScheme"
describe('bank payment scheme', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        // Step 01 :  Access
        cy.viewport(1920, 1080)
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    });
    
    it('Test delete payment scheme', ()=>{
      cy.get('#\\35 22408934932516876 > :nth-child(3) > .t-LinksList-link').click();
      cy.get('#myreg_ig_grid_vc_cur > a').click()
      cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
      .invoke('css' ,'height', '800px')
      cy.handleModal()
      cy.getIframeDom('#B298303337932060584').should('be.visible').and('have.text','Delete').click()
    })

})
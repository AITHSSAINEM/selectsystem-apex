/// <reference types="cypress"/>
import {MENU} from '../../../../loctors/menu'
import { NEW_CENTER , ADDRESS , UI_DIALOG, FIRS_CONTACT, SEC_CONTACT, CORRESPENDANCE } from '../../../../loctors/center'
describe('Center Commission ',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#myreg_data_tab > a > span').should('be.visible').and('have.text','Center Menu').click()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(2) > a').should('be.visible').contains('Center Commission').click()
    })

    it(' Update center commission  - Nominal case ', ()=>{

        cy.get('#myreg_ig_grid_vc_cur > a').click();
        cy.get('#P100302_COM_RATE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper').click();
        //cy.get('#P100302_COM_IDEN').clear().type('1234567');
        cy.get('#P100302_COM_RATE').clear().type('1');
        cy.get('#P100302_COM_MIN').clear().type('6');
        cy.get('#P100302_COM_LABE').clear().type('updated');
        cy.get('#P100302_COM_FIXE').clear().type('12');
        cy.get('#P100302_COM_MAX').clear().type('47');
        cy.get('#B234441970728367453').should('be.visible');
        cy.get('#B234441970728367453').should('have.text', 'Apply Changes').click();

    })

})
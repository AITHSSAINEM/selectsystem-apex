/// <reference types='cypress'/>
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";

describe('Bank currency rate - nominal case ', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        // Step 01 :  Access
        cy.viewport(1920, 1080)
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    });
    
    it('Test add bank\'scurrency rate - Nominal case.',()=>{
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#\\35 22408934932516876 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('.is-selected > :nth-child(2) > a > .fa').click();
        cy.handleModal()
        cy.getIframeDom('#\\32 35028513001019165 > li:nth-child(2) > a > span.t-LinksList-label').click()
        cy.get('#B364395464901515636').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','500px')  
        cy.getIframeDom('#P900227_BCC_CUR_SECO_CODE_lov_btn').click()
       
        cy.get('#PopupLov_900227_P900227_BCC_CUR_SECO_CODE_dlg > div.a-PopupLOV-searchBar > input').type('US Dollar{enter}')   
        cy.get('#PopupLov_900227_P900227_BCC_CUR_SECO_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste').first().children().first().click()
            }
          })

        cy.getIframeDom('#P900227_BCC_SYS_CODE_lov_btn').click()

        cy.get('#PopupLov_900227_P900227_BCC_SYS_CODE_dlg > div.a-PopupLOV-searchBar > input').type('CTMI{enter}')
        cy.get('#PopupLov_900227_P900227_BCC_SYS_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste2').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste2').first().children().first().click()
            }
          })

        cy.getIframeDom('#P900227_BCC_RATE').type('10')
        cy.getIframeDom('#P900227_BCC_EFFE_DATE_input').type('28-10-2022')
        cy.getIframeDom('#P900227_BCC_INTG_DATE_input').type('26-10-2022{enter}')
        cy.getIframeDom('#B364422096003536654').should('be.visible').click()
        cy.get('#t_Alert_Success').should('be.visible')
    })

    it('Test add bank\' currency rate - Mandatory fields left empty -Exptionl case.', ()=>{
        cy.get('#\\35 22408934932516876 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('.is-selected > :nth-child(2) > a > .fa').click();
        cy.handleModal()
        cy.getIframeDom('#\\32 35028513001019165 > li:nth-child(2) > a > span.t-LinksList-label').click()
        cy.get('#B364395464901515636').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','500px') 

        // les divs des erreurs 
        cy.getIframeDom('#B364422096003536654').should('be.visible').click()
        cy.getIframeDom('#P900227_BCC_SYS_CODE_error').should('be.visible')
        cy.getIframeDom('#P900227_BCC_CUR_SECO_CODE_error').should('be.visible')
        cy.getIframeDom('#P900227_BCC_RATE_error').should('be.visible')
        cy.getIframeDom('#P900227_BCC_EFFE_DATE_error').should('be.visible')
        cy.getIframeDom('#P900227_BCC_INTG_DATE_error').should('be.visible')
      
      
        cy.getIframeDom('#t_Alert_Notification').should('be.visible')

    })
    it('Test add bank\'s currency rate- Integration date greater than effective date - Exptional case.',()=>{
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#\\35 22408934932516876 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('.is-selected > :nth-child(2) > a > .fa').click();
        cy.handleModal()
        cy.getIframeDom('#\\32 35028513001019165 > li:nth-child(2) > a > span.t-LinksList-label').click()
        cy.get('#B364395464901515636').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','500px')  
        cy.getIframeDom('#P900227_BCC_CUR_SECO_CODE_lov_btn').click()
       
        cy.get('#PopupLov_900227_P900227_BCC_CUR_SECO_CODE_dlg > div.a-PopupLOV-searchBar > input').type('US Dollar{enter}')   
        cy.get('#PopupLov_900227_P900227_BCC_CUR_SECO_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste').first().children().first().click()
            }
          })

        cy.getIframeDom('#P900227_BCC_SYS_CODE_lov_btn').click()

        cy.get('#PopupLov_900227_P900227_BCC_SYS_CODE_dlg > div.a-PopupLOV-searchBar > input').type('CTMI{enter}')
        cy.get('#PopupLov_900227_P900227_BCC_SYS_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste2').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste2').first().children().first().click()
            }
          })

        cy.getIframeDom('#P900227_BCC_RATE').type('10')
        cy.getIframeDom('#P900227_BCC_EFFE_DATE_input').type('28-10-2022')
        cy.getIframeDom('#P900227_BCC_INTG_DATE_input').type('30-10-2022{enter}')
        cy.getIframeDom('#B364422096003536654').should('be.visible').click()
        cy.getIframeDom('#t_Alert_Notification > div').should('be.visible')
        cy.getIframeDom('#t_Alert_Notification > div > div.t-Alert-content').should('be.visible').contains('Effective Date must be greater or equal to Integration Date')
    })
})
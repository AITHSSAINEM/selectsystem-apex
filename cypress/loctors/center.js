export const NEW_CENTER = {
   BASE_PARAM : "#\\31 36873271293040706 > li:nth-child(1)",
   CENTER_ITEM : "#\\35 22400360937447804 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label",
   CENTER_ID : "#P900201_CEN_IDEN", 
   CENTER_NAME : "#P900201_CEN_CORP_NAME",
   CENTER_BTN_SEAR : "#P900201_CEN_ACQU_CURR_CODE_lov_btn > .a-Icon",
   CENTER_INP_SEAR : ".a-PopupLOV-search",
   LIST_REF : "#PopupLov_900201_P900201_CEN_ACQU_CURR_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul",
   CENTER_INITIAL : "#P900201_CEN_INIT",
   LICENCE_NUM : "#P900201_CEN_LICE_NUMB",
   SOCIAL_NUM : "#P900201_CEN_CNSS_NUMB",
   SERIE_NUM : "#P900201_CEN_SIRE_NUMB",
   CAPITAL : "#P900201_CEN_CAPI",
   COM_REGI_NUM : "#P900201_CEN_COMM_REGI_NUMB",
   TIME_ZONE_SEL : "#P900201_CEN_SIGN_TIME_ZONE",
   TIME_ZONE_INP : "#P900201_CEN_TIME_ZONE"
}

export const ADDRESS  = {
    ADDRESS_TYPE : "#C225544191210469005_HDR",
    ADDRESS1 : "#C223995915192068635_HDR",
    ADDRESS2 :"#C223996024052068636_HDR",
    CITY: "#AdrReg_ig_grid_vc_cur",
    PHONE : "#C223996616036068642_HDR",
    FAX : "#AdrReg_ig_grid_vc_cur", 
    ADD_BTN : "#B225546235467469026"
}
export const UI_DIALOG = {

    ADDRESS : "#P900203_ADR_ADR1",
    ADDRESS2 : "#P900203_ADR_ADR2",
    ZIP : "#P900203_ADR_ADRE_ZIPD",
    STREET : "#P900203_ADR_ADRE_STRE",
    PO_BOX : "#P900203_ADR_ADRE_POBX",
    STATE :"#P900203_ADR_ADRE_STAT",
    PHONE1 : "#P900203_ADR_PHO1",
    PHONE2 : "#P900203_ADR_PHO2",
    // type address
    TYPE_BTN : "#P900203_ADR_ATY_CODE_lov_btn",
    TYPE_INP : "#PopupLov_900203_P900203_ADR_ATY_CODE_dlg > div.a-PopupLOV-searchBar > input",
    TYPE_LIST : "#PopupLov_900203_P900203_ADR_ATY_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul",
    
    //CITY 
    CITY_BTN : "#P900203_ADR_CITY_CODE_lov_btn",
    CITY_INP : "#PopupLov_900203_P900203_ADR_CITY_CODE_dlg > div.a-PopupLOV-searchBar > input",
    CITY_LIST: "#PopupLov_900203_P900203_ADR_CITY_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul",
    FAX :"#P900203_ADR_FAX", 
    CREATE_BTN :"#B226369560741288396"
    }

    export const FIRS_CONTACT = {
        FIRS_NAME : "#P900203_ADR_FIRS_FIRS_NAME", 
        LAST_NAME : "#P900203_ADR_FIRS_LAST_NAME",
        FUNCTION : "#P900203_ADR_FIRS_FUNC",
        PHONE : "#P900203_ADR_FIRS_PHON",
        EMAIL :" #P900203_ADR_FIRS_MAIL"
        }
        
     export const SEC_CONTACT = {
        FIRS_NAME : "#P900203_ADR_SECO_FIRS_NAME", 
        LAST_NAME : "#P900203_ADR_SECO_LAST_NAME",
        FUNCTION : "#P900203_ADR_SECO_FUNC",
        PHONE : "#P900203_ADR_SECO_PHON",
        EMAIL :" #P900203_ADR_SECO_MAIL"
        }

    export const CORRESPENDANCE = {
        ADD_BTN : "#R225546586182469029_ig_toolbar > div.a-Toolbar-groupContainer.a-Toolbar-groupContainer--start > div:nth-child(6) > button",
        TYPE_CORR : '#C225546866890469032',
        ADDRESS_TYPE : '#C225546908252469033',
        MODE_COM : '#C225547042943469034', 
        SAVE_BTN : "#R225546586182469029_ig_toolbar_m2"
    }
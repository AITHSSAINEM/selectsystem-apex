/// <reference types ="cypress" />
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import { ACCESS, BANK_DATA, BANK_CURRENCY, PARAMETER ,DEFAULT_ACCOUNT , SUMMARY} from "../../../loctors/bank/bank";
import { ERROR } from "../../../loctors/bank/errors";
describe('Create BANK', ()=>{
    
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        cy.viewport(1920, 1080)
        // Step 01 :  Access
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(ACCESS.BANK_MANAGEMENT_ITEM).click()
    
    })

    it('add bank - Nominal case.', ()=>{

      cy.get(ACCESS.ADD_BANK_BTN).click()
      cy.get(ACCESS.BANK_IFRAM).invoke('css', 'height', '800px')
      cy.handleModal()
        // Step 02 : Bank data 
    cy.getIframeDom(BANK_DATA.ID).type("11224")
    cy.getIframeDom(BANK_DATA.COUNTRY_BTN_SEAR).click()
    cy.get(BANK_DATA.COUNTRY_INP_SEAR).type('Angola{enter}')
    cy.get(BANK_DATA.COUNTRY).first().then(($ul) => {
        // Vérifiez si l'élément ul existe
        if ($ul.length) {
          // Sélectionnez le premier enfant de ul
          cy.get(BANK_DATA.COUNTRY).first().children().first().click()
        }
      })
    cy.getIframeDom(BANK_DATA.CORPORATE_NAME).type('AFRILAND FIRST BANK')
    cy.getIframeDom(BANK_DATA.INITIALS).type('AFB')
    cy.getIframeDom(BANK_DATA.TYPE).select('61')

    cy.getIframeDom(BANK_DATA.GROUP_BTN_SEAR).click()
    cy.get(BANK_DATA.GROUP_INP_SEAR).type('ONUS GROUP{enter}')
    
    cy.get(BANK_DATA.GROUP).first().then(($ul) => {
        // Vérifiez si l'élément ul existe
        if ($ul.length) {
          // Sélectionnez le premier enfant de ul
          cy.get(BANK_DATA.GROUP).first().children().first().click()
        }
      })

    cy.getIframeDom(BANK_DATA.NEXT_BTN).click()

    // step 03 : Bank currency
    cy.handleModal()
    cy.getIframeDom(BANK_CURRENCY.REF_CURR).select('40')
    cy.getIframeDom(BANK_CURRENCY.ORIGINE_BANK).click({force: true})
    cy.getIframeDom(BANK_CURRENCY.NEXT_BTN).click()

    // step 04 : Parameters 
    cy.handleModal()
    cy.getIframeDom(PARAMETER.MATC_AUTH_DAYS).clear().type('30')
    cy.getIframeDom(PARAMETER.ACOUNT_NUMBER_BTN).click()
    cy.get(PARAMETER.ACOUNT_NUMBER_INP).type('Serial Method{enter}')
    
    cy.get(PARAMETER.ACOUNT_NUMBER).first().then(($ul) => {
        // Vérifiez si l'élément ul existe
        if ($ul.length) {
          // Sélectionnez le premier enfant de ul
          cy.get(PARAMETER.ACOUNT_NUMBER).first().children().first().click()
        }
      })
     cy.getIframeDom(PARAMETER.BAN_MATC_PERC).clear().type('10')
     cy.getIframeDom(PARAMETER.NEXT_BTN).click()

     // Step 05 : Default account 
     cy.handleModal()
     cy.getIframeDom(DEFAULT_ACCOUNT.BANK_ACCOUNT).clear().type('9844654354164')
     cy.getIframeDom(DEFAULT_ACCOUNT.MPX_ACCOUNT).clear().type('494543198498746')
     cy.getIframeDom(DEFAULT_ACCOUNT.CURRENCY_BTN).click()
     cy.get(DEFAULT_ACCOUNT.CURRENCY_INP).clear().type('CFA Franc{enter}')

     cy.get(DEFAULT_ACCOUNT.CURRENCY).first().then(($ul) => {
        // Vérifiez si l'élément ul existe
        if ($ul.length) {
          // Sélectionnez le premier enfant de ul
          cy.get(DEFAULT_ACCOUNT.CURRENCY).first().children().first().click()
        }
      })

     cy.getIframeDom(DEFAULT_ACCOUNT.NEXT_BTN).click()

     // step 06 : SUMMARY 
     cy.handleModal()
     // les assertions sur les sections de summary
     Object.keys(SUMMARY).forEach(key => {
     cy.getIframeDom(SUMMARY[key]).scrollIntoView().should('be.visible')
     })
      

     // pour creer une banque, il suffit d'activer la ligne suivante : 
      cy.getIframeDom(SUMMARY.CREATE_BTN).click()
    })
    
    it('add bank - Required Fields Empty -', ()=>{
      cy.get(ACCESS.ADD_BANK_BTN).click()
      cy.get(ACCESS.BANK_IFRAM).invoke('css', 'height', '800px')
      cy.handleModal()
      cy.getIframeDom(BANK_DATA.NEXT_BTN).click()
      
    
      Object.keys(ERROR).forEach(key => {
        cy.getIframeDom(ERROR[key]).scrollIntoView().should('be.visible')
        })
       /*
       const data = ['Id must have some value.','Country must have some value.','Bank Corporate Name must have some value','Type must have some value.','']
       const errorKeys = Object.keys(ERROR);
       
       for (let i = 0; i < errorKeys.length - 1; i++) {
          const key = errorKeys[i];
          cy.getIframeDom(ERROR[key]).contains(data[i]);
        }
        */
    })
     
})
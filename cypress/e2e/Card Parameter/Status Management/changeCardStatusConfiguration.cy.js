import {MENU} from '../../../loctors/menu'
describe('Status Management.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Change Card Status Configuration-Nominal case ',()=>{
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link').click();
        cy.get('#\\36 66871638801445837 > :nth-child(8) > .t-LinksList-link > .t-LinksList-label').click();
        
        cy.get('#P101112_CARD_STATUS').select('Active');
        cy.get('#P101112_ALLOWED_STATUS_LEFT').should('have.attr', 'size', '5');
        cy.get('#P101112_ALLOWED_STATUS_RIGHT').should('have.attr', 'size', '5');
        cy.get('#P101112_ALLOWED_STATUS_MOVE_ALL > .a-Icon').click();
        cy.get('#P101112_ALLOWED_STATUS_MOVE > .a-Icon').click();
        cy.get('#P101112_ALLOWED_STATUS_REMOVE > .a-Icon').click();
        cy.get('#P101112_ALLOWED_STATUS_REMOVE_ALL > .a-Icon').click();
        cy.get('#P101112_ALLOWED_STATUS_LEFT').select(['Blocked']);
        cy.get('#P101112_ALLOWED_STATUS_MOVE > .a-Icon').click();
        cy.get('#P101112_ALLOWED_STATUS_LEFT').select(['Blockage']);
        cy.get('#P101112_ALLOWED_STATUS_MOVE > .a-Icon').click();
        cy.get('#P101112_ALLOWED_STATUS_RIGHT').select(['Blocked']);
        cy.get('#P101112_ALLOWED_STATUS_MOVE').click();
        cy.get('#P101112_ALLOWED_STATUS_RESET > .a-Icon').click();
        cy.get('#B552271224404206429 > .t-Button-label').should('be.visible');
        
    })
})
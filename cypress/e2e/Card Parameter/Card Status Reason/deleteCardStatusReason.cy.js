
/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
describe('Card Status Reason.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('UpateCard Status Reason -Nominal case ',()=>{
        
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#\\36 66871638801445837 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#myreg_ig_grid_vc_cur > a').click()
        
  
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
         .invoke('css','height', '350px')
        cy.handleModal()
         
        cy.getIframeDom('#B229773795299692471').should('be.visible').click()
        cy.get('.js-confirmBtn').should('be.visible');
        cy.get('.js-confirmBtn').click();
        cy.get('#t_Alert_Success').should('be.visible')
       
       

    })
})
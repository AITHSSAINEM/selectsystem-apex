export const CITY = {
    ADD_BTN :"#mybtn",
    IDEN : "#P900107_CIT_IDEN",
    NAME : "#P900107_CIT_LABE",
    SERACH_BTN : "#P900107_CIT_COUN_CODE_lov_btn",
    SERACH_INP : "#PopupLov_900107_P900107_CIT_COUN_CODE_dlg > div.a-PopupLOV-searchBar > input",
    CREATE_BTN : "#B77746349108386985",
    CANCEL_BTN :"#B77744164539386984",
    PENCIL_ICON : "#myreg_ig_grid_vc_cur > a",
    APPLY_CHANGES: "#B77745959289386985",
    DELETE_BTN : "#B77745585955386985",
    COUNTRY : "#PopupLov_900107_P900107_CIT_COUN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul"
    
}
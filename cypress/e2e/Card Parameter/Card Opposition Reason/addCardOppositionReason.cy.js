import {MENU} from '../../../loctors/menu'
describe('Add Card Opposition Reason.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Add Card Opposition Reason -Nominal case ',()=>{
        
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#\\36 66871638801445837 > :nth-child(5) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('.t-Button-label').click();


        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height', '320px')
        cy.handleModal()
         

        cy.getIframeDom('#P101039_OMO_IDEN').type('1633')
        cy.getIframeDom('#P101039_OMO_LABE').type('Closed Account')
        cy.getIframeDom('#B304321270697963927').should('be.visible').click()
        
    })
})
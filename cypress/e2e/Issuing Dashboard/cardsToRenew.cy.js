/// <reference types="cypress" />
import { DASHBOARD_PAGE,CARD_RENEW } from '../../loctors/dashboard'
describe('Issuing dashoard - Cards To Renew -',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        // going to dashboard > issuing.
        cy.get(DASHBOARD_PAGE.DASHBOARD_ITEM).click()
        cy.get(DASHBOARD_PAGE.ISSUING_ITEM).click()
    })

    it('Cards To Renew - All Banks - All Card Types - All Renewal Modes - All Branches', ()=>{
        cy.get(CARD_RENEW.CARDS_RENEW_TAB).contains('Cards To Renew').click()
        cy.get(CARD_RENEW.TITLE).contains('Cards To Renew').should('be.visible')
        cy.get(CARD_RENEW.CARDS_RENEW_DASHBOARD).should('be.visible')
        cy.get(CARD_RENEW.BANK_CARDS_TO_RENEW).select('').contains('--ALL--')
        cy.get(CARD_RENEW.CARD_TYPE_CARDS_TO_RENEW).select('').contains('--ALL--')
        cy.get(CARD_RENEW.GROUP_BY_RENEWAL_MODE).contains('Renewal Mode').click()
        cy.get(CARD_RENEW.RENEWAL_MODE_DIAGRAM).should('be.visible')
    })
    it('Cards To Renew - One Bank - One Card Type - One Renewal Mode - All branch - ', ()=>{
        cy.get(CARD_RENEW.CARDS_RENEW_TAB).contains('Cards To Renew').click()
        cy.get(CARD_RENEW.TITLE).contains('Cards To Renew').should('be.visible')
        cy.get(CARD_RENEW.CARDS_RENEW_DASHBOARD).should('be.visible')
        cy.get(CARD_RENEW.BANK_CARDS_TO_RENEW).select('20').contains('BICICI')
        cy.get(CARD_RENEW.CARD_TYPE_CARDS_TO_RENEW).select('1').contains('Debit')
        cy.get(CARD_RENEW.GROUP_BY_RENEWAL_MODE).contains('Renewal Mode').click()
        cy.get(CARD_RENEW.RENEWAL_MODE_DIAGRAM).should('be.visible') 
    })
    
})
/// <reference types ="cypress" />
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import { ACCESS, BANK_DATA, BANK_CURRENCY, PARAMETER ,DEFAULT_ACCOUNT , SUMMARY} from "../../../loctors/bank/bank";
import { ERROR } from "../../../loctors/bank/errors";
describe('Display Ban List', ()=>{
    
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        cy.viewport(1920, 1080)
        // Step 01 :  Access
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(ACCESS.BANK_MANAGEMENT_ITEM).click()
    
    })

    it('Test display bank list', ()=>{
        cy.get('table tbody tr')  // sélectionne les lignes de la table
        .should('have.length.gt', 0)  // vérifie qu'il y a plus de 0 lignes 
    })
})
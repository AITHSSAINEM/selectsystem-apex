 export  const ACCESS = {
    BANK_MANAGEMENT_ITEM : "#\\35 22408934932516876 > :nth-child(1) > .t-LinksList-link",
    ADD_BANK_BTN : "#mybtn", 
    BANK_IFRAM : "#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--wizard.ui-draggable"
 }

 export const BANK_DATA = {
    ID: "#P902206_BAN_IDEN",
    COUNTRY_BTN_SEAR : "#P902206_BAN_ACQU_COUN_CODE_lov_btn",
    COUNTRY_INP_SEAR : "#PopupLov_902206_P902206_BAN_ACQU_COUN_CODE_dlg > div.a-PopupLOV-searchBar > input",
    COUNTRY: "#PopupLov_902206_P902206_BAN_ACQU_COUN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul",
    CORPORATE_NAME : "#P902206_BAN_CORP_NAME",
    INITIALS : "#P902206_BAN_INIT",
    TYPE : "#P902206_BAN_BANK_TYPE_CODE",
    GROUP_BTN_SEAR: "#P902206_BAN_BGR_CODE_lov_btn",
    GROUP_INP_SEAR: "#PopupLov_902206_P902206_BAN_BGR_CODE_dlg > div.a-PopupLOV-searchBar > input",
    GROUP :  "#PopupLov_902206_P902206_BAN_BGR_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul",
    NEXT_BTN : "#B298261031120972882",
    }

    
export const BANK_CURRENCY = {
    REF_CURR :"#P902207_BAN_ACQU_CURR_CODE", 
    ORIGINE_BANK :"#P902207_BAN_ORIG_CURR_RATE_0",
    ORIGINE_CENTRE :"#P902207_BAN_ORIG_CURR_RATE_1",
    NEXT_BTN :"#B298265330767972888"
}
export const PARAMETER = {
  MATC_AUTH_DAYS : "#P902208_BAN_NON_MATC_AUTH_DAYS",
    BAN_MATC_PERC : "#P902208_BAN_MATC_PERC",
    NEXT_BTN :"#B298270367919972890",
    ACOUNT_NUMBER_BTN : "#P902208_BAN_ANG_CODE_lov_btn",
    ACOUNT_NUMBER_INP :  "#PopupLov_902208_P902208_BAN_ANG_CODE_dlg > div.a-PopupLOV-searchBar > input",
    ACOUNT_NUMBER: "#PopupLov_902208_P902208_BAN_ANG_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul"
 }

 export const DEFAULT_ACCOUNT = {
    BANK_ACCOUNT : "#P902209_BNK_ACCOUNT",
    MPX_ACCOUNT : "#P902209_MXP_ACCOUNT",
    CURRENCY_BTN: "#P902209_CURRENCY_lov_btn",
    CURRENCY_INP : "#PopupLov_902209_P902209_CURRENCY_dlg > div.a-PopupLOV-searchBar > input",
    CURRENCY :"#PopupLov_902209_P902209_CURRENCY_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul",
    NEXT_BTN :"#B298447244416388721"
   }

 export const  SUMMARY = {
  CREATE_BTN : "#B298456784791441276",
  BANK_DATA_SEC :"#R597092226460524585 > div.t-Region-bodyWrap > div.t-Region-body",
  CURRENCY_SEC :"#R597099205407526300 > div.t-Region-bodyWrap > div.t-Region-body",
  PARAMETERS_SEC : "#R597105333749528048 > div.t-Region-bodyWrap > div.t-Region-body",
  DEFAULT_ACCOUNT_SEC :"#R895156261546771846 > div.t-Region-bodyWrap > div.t-Region-body"
 }
/// <reference types ="cypress" />
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import { ACCESS} from "../../../loctors/bank/bank";
import { Commission, Revolving, Settlement} from "../../../loctors/bank/configure";
import {  UI_DIALOG, FIRS_CONTACT, SEC_CONTACT } from '../../../loctors/center'

describe('Create BANK', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        // Step 01 :  Access
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(ACCESS.BANK_MANAGEMENT_ITEM).click()
        cy.get(Commission.PENCIL).click();
    })

    it('Configure Bank\'s Center Commission - Nominal Case', ()=>{
        cy.get(Commission.LINK).click();
        cy.get(Commission.BOTH).select('302');
        cy.get(Commission.ISSUER).select('302');
        cy.get(Commission.ACQUIRER).select('302');
        cy.get(Commission.APPLY_CHANGES).should('be.visible');

        // Pour sauvegarder les modifications, il suffit d'activer la lige suivante 
       // cy.get(Commission.APPLY_CHANGES).click()
    })


    it('Configure Bank\'s Revolving Date - Nominal Case', function() {
        cy.get(Revolving.LINK).click();
        cy.get(Revolving.LAST_BUS_DATE_BTN).click();
        cy.get(Revolving.LAST_BUS_DATE_YEAR).select('2022');
        cy.get(Revolving.LAST_BUS_DATE_MONTH).select('9');
        cy.get(Revolving.LAST_BUS_DATE_DAY).click();
        cy.get('#P902203_BAN_LAST_BUSI_DATE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper').click();
        cy.get(Revolving.BUS_DATE_BTN).click();
        cy.get(Revolving.BUS_DATE_YEAR).select('2022');
        cy.get(Revolving.BUS_DATE_MONTH).select('10');
        cy.get(Revolving.BUS_DATE_DAY).click();
        cy.get('#main').click();
        cy.get(Revolving.APPLY_CHANGES).should('be.visible');

        // Pour appliquer les modifications ci-dissus, il suffit d'activer la ligne suivante : 
       //cy.get(Revolving.APPLY_CHANGES).click()
        
    });

    it('Add Bank\'s Settlement Account - Nominal Case', ()=>{
        cy.get(Settlement.LINK).click();
        cy.get(Settlement.ADD_ROW_BTN).click();
        cy.get(Settlement.IFRAME).invoke('css' , 'height','250px')
        cy.handleModal()
        cy.getIframeDom(Settlement.MXP_ACCOUT).type('9486746549485')
        cy.getIframeDom(Settlement.BANK_ACCOUNT).type('37627667575')
        cy.getIframeDom(Settlement.CURRENCY).select('39')
        cy.getIframeDom(Settlement.CREATE_BTN).should('be.visible').and('have.text', 'Create')

        // Pour ajouter une compte de règlement bancaire, il suffit d'activer la ligne suivante : 
        // cy.getIframeDom(Settlement.CREATE_BTN).click()
    })

    it('Add Bank\'s Address - Nominal Case', ()=>{
        cy.get('#R870624447908623106_tab > a > span').click();
        cy.get('#mycre > .t-Button-label').click();
        cy.get("#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable")
        .invoke('css', "height" , "800px")
        cy.handleModal()
        cy.getIframeDom(UI_DIALOG.TYPE_BTN).click()
        cy.get(UI_DIALOG.TYPE_INP).type("01 - Mail Address{enter}")
        cy.get(UI_DIALOG.TYPE_LIST).first().then(($ul) => {
          // Vérifiez si l'élément ul existe
          if ($ul.length) {
            // Sélectionnez le premier enfant de ul
            cy.get(UI_DIALOG.TYPE_LIST).first().children().first().click()
          }
        })
        cy.getIframeDom(UI_DIALOG.ADDRESS).type('103 Zerktouni Avenue')
        cy.getIframeDom(UI_DIALOG.ADDRESS2).type('5th floor, Door 5')
        cy.getIframeDom(UI_DIALOG.STREET).type('Abidjan Street')
        cy.getIframeDom(UI_DIALOG.PO_BOX).type('54646')
        cy.getIframeDom(UI_DIALOG.ZIP).type('45100')
        cy.getIframeDom(UI_DIALOG.CITY_BTN).click()
        cy.get(UI_DIALOG.CITY_INP).type("Abidjan{enter}")
        cy.get(UI_DIALOG.CITY_LIST).first().then(($ul) => {
          // Vérifiez si l'élément ul existe
          if ($ul.length) {
            // Sélectionnez le premier enfant de ul
            cy.get(UI_DIALOG.CITY_LIST).first().children().first().click()
          }
        })
        cy.getIframeDom(UI_DIALOG.STATE).type('384')
        cy.getIframeDom(UI_DIALOG.PHONE1).type('0198765432')
        cy.getIframeDom(UI_DIALOG.PHONE2).type('0198765473')
        cy.getIframeDom(UI_DIALOG.FAX).type('0150765444')
        cy.getIframeDom('#R225546316643469027').should('be.visible')
  
        // remplir les champs des deux contacts.
  
        // First Conctact 
  
        cy.getIframeDom(FIRS_CONTACT.FIRS_NAME).type('Ahmed')
        cy.getIframeDom(FIRS_CONTACT.LAST_NAME).type('Ali')
        cy.getIframeDom(FIRS_CONTACT.FUNCTION).type('Agent')
        cy.getIframeDom(FIRS_CONTACT.PHONE).type('0165478498')
        cy.getIframeDom(FIRS_CONTACT.EMAIL).type('ahmed.ali@bicici.ci')
  
        // Second Contact
       cy.getIframeDom(SEC_CONTACT.FIRS_NAME).type('Mostapha')
       cy.getIframeDom(SEC_CONTACT.LAST_NAME).type('Mohamed')
       cy.getIframeDom(SEC_CONTACT.FUNCTION).type('Agent')
       cy.getIframeDom(SEC_CONTACT.PHONE).type('0165478498')
       cy.getIframeDom(SEC_CONTACT.EMAIL).type('m.mohamed@bicici.ci')
       cy.getIframeDom('#B226369560741288396').should('be.visible').and('have.text', 'Create')
    })
})
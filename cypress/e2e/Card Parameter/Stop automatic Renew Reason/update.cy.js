
/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
describe('Card Parameter.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Update Stop Automatic Renew Reason.', ()=>{
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link').click();
        cy.get('#\\36 66871638801445837 > :nth-child(4) > .t-LinksList-link > .t-LinksList-label').click();
        //cy.get('#myreg_ig_grid_vc_cur > a').click({force: true})

        cy.xpath('/html/body/form/div[1]/div/div[2]/main/div[2]/div/div/div/div/div[2]/div[5]/div[3]/div[1]/div[3]/div[4]/table/tbody/tr[1]/td[4]/a')
        .click()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height', '300px')
        cy.handleModal()

        cy.getIframeDom('#P101027_SAR_BAN_CODE_lov_btn').click()
         cy.get('#PopupLov_101027_P101027_SAR_BAN_CODE_dlg > div.a-PopupLOV-searchBar > input')
         .type('BICICI{enter}')

         cy.get('#PopupLov_101027_P101027_SAR_BAN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste').first().then(($ul) => {
             // Vérifiez si l'élément ul existe
             if ($ul.length) {
               // Sélectionnez le premier enfant de ul
               cy.get('@liste').first().children().first().click()
             }
           })
         cy.getIframeDom('#P101027_SAR_IDEN').clear().type('001')
         cy.getIframeDom('#P101027_SAR_LABE').clear().type('CARTE ENDOMMAGE')

        cy.getIframeDom('#B230971222647088854').should('be.visible').click()
        cy.get('#t_Alert_Success').should('be.visible')

    })
})
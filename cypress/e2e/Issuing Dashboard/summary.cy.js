/// <reference types="cypress" />
import { DASHBOARD_PAGE } from '../../loctors/dashboard'
describe('TEST DASHBOARD - Nominal Case', ()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(DASHBOARD_PAGE.DASHBOARD_ITEM).click()
        cy.get(DASHBOARD_PAGE.ISSUING_ITEM).click()
    })
       afterEach(()=>{
        cy.wait(500)
       })
    it('The "Cards" section must show the "Total number of Cards".', () => {
        cy.get(DASHBOARD_PAGE.CARD_SECTION).should('be.visible')
        cy.get(DASHBOARD_PAGE.CARDS_TOTAL).should('be.visible')
    });

    it('The "Accounts" section must show the "Total number of Accounts".', () => {
        cy.get(DASHBOARD_PAGE.ACCOUNT_SECTION).should('be.visible')
        cy.get(DASHBOARD_PAGE.ACCOUNTS_TOTAL).should('be.be.visible')
    });

    it('The "Customers" section must show the "Total number of Customers".', () => {
        cy.get(DASHBOARD_PAGE.CUSTOMER_SECTION).should('be.visible')
        cy.get(DASHBOARD_PAGE.CUSTOMERS_TOTAL).should('be.visible')
    });
})
/// <reference types ="cypress" />
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import { ACCESS} from "../../../loctors/bank/bank";
import { Commission, Revolving, Settlement} from "../../../loctors/bank/configure";
import {  UI_DIALOG, FIRS_CONTACT, SEC_CONTACT } from '../../../loctors/center'

describe('Create BANK', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        // Step 01 :  Access
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(ACCESS.BANK_MANAGEMENT_ITEM).click()
        cy.get(Commission.PENCIL).click();
    })

     it('Add Bank\'s Correspondance List- Nominal case.',()=>{
       cy.get('#R502303951894942816_tab > a ').click()

       /* ==== Generated with Cypress Studio ==== */
       cy.get('#R502303951894942816_ig_toolbar > .a-Toolbar-groupContainer--start > :nth-child(6) > .a-Button > .a-Button-label').click();
       cy.get('#C234335055180918530_lov_btn > .a-Icon').click();
       cy.get('.a-PopupLOV-search').type('Pinmailers{enter}');
       cy.get('#PopupLov_902203_C234335055180918530_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('list').first().then(($ul) => {
          // Vérifiez si l'élément ul existe
          if ($ul.length) {
            // Sélectionnez le premier enfant de ul
            cy.get("@list").first().children().first().click()
          }
        })




       cy.get('#R502303951894942816_ig_grid_vc_cur').next().click();

       cy.get('#C234335180567918531_lov_btn > .a-Icon').click();
       cy.get('#PopupLov_902203_C234335180567918531_dlg > .a-PopupLOV-searchBar > .a-PopupLOV-search').clear();
       cy.get('#PopupLov_902203_C234335180567918531_dlg > .a-PopupLOV-searchBar > .a-PopupLOV-search').type('Registered Office{enter}');
       cy.get('#PopupLov_902203_C234335180567918531_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('list2').first().then(($ul) => {
        // Vérifiez si l'élément ul existe
        if ($ul.length) {
          // Sélectionnez le premier enfant de ul
          cy.get("@list2").first().children().first().click()
        }
      })

       cy.get('#R502303951894942816_ig_grid_vc_cur').next().click();
       cy.get('#C234335299052918532').select('Letter');
       cy.get('#B231837788870467778 > .t-Button-label').should('be.visible').click();
       
     })
})

/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Personalization Parameters.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    })

    it('Configure Personalization Pre-Paid. - Nominal case.', ()=>{
        cy.get('#\\35 23400178171740781 > :nth-child(2) > .t-LinksList-link').click();
        cy.get('#R182223024764852463_ig_grid_vc_cur > a > :nth-child(2)').click();
        cy.get('b').click();
        cy.get('#R163648554785320818_tab > a > span').click();
        cy.get('#P900211_PPA_PRE_PAID_TARG_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_PPA_ANON_STAT_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_PPA_PRE_PAID_PROG_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_PPA_BRA_CODE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_PPA_BRA_CODE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#B128787068630191877 > .t-Button-label').should('be.visible');
    })
})
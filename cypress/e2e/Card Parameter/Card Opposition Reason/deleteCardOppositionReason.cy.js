import {MENU} from '../../../loctors/menu'
describe('Add Card Opposition Reason.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Delete Card Opposition Reason -Nominal case ',()=>{

        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#\\36 66871638801445837 > :nth-child(5) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#myreg_ig_grid_vc_cur > a > .apex-edit-pencil-alt').click();

        
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height', '320px')
        cy.handleModal()
        cy.getIframeDom('#B304320446709963927').should('be.visible').click()
        cy.get('.js-confirmBtn').should('be.visible');
        cy.get('.js-confirmBtn').click();

       // cy.get('#t_Alert_Success').should('be.visible')
         
    })
})
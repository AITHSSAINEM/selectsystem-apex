/// <reference types="cypress"/>
import {MENU} from '../../../../loctors/menu'
import { NEW_CENTER , ADDRESS , UI_DIALOG, FIRS_CONTACT, SEC_CONTACT, CORRESPENDANCE } from '../../../../loctors/center'
describe('Center : Interchange commission',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
    })

    it('Add interchange Commission   - Nominal case ', ()=>{
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(3) > a').click()
        cy.get('#mybtn').click()
        cy.get('#P100302_COM_IDEN').type('0005');
        cy.get('#P100302_COM_RATE').type('2');
        cy.get('#P100302_COM_MIN').type('12');
        cy.get('#P100302_COM_LABE').type('Interchange Commission');
        cy.get('#P100302_COM_FIXE').type('14');
        cy.get('#P100302_COM_MAX').type('20');
        cy.get('#B234442319461367453').should('be.visible').and('have.text','Add New Commission').click()
    })

    it('Add interchange Commission - "Iden" field empty - Exptional case  ', ()=>{
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(3) > a').click()
        cy.get('#mybtn').click()
        cy.get('#P100302_COM_IDEN').clear();
        cy.get('#P100302_COM_RATE').type('2');
        cy.get('#P100302_COM_MIN').type('12');
        cy.get('#P100302_COM_LABE').type('Interchange Commission');
        cy.get('#P100302_COM_FIXE').type('14');
        cy.get('#P100302_COM_MAX').type('20');
       // cy.get('#B234442319461367453').should('be.visible').and('have.text','Add New Commission').click()
    })
})
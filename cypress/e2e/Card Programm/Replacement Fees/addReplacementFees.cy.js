/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { REPLACEMENT } from '../../../loctors/card program/replacementFees'
describe('Replacement Fees .',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })


    it('Add Replacement Fees-Nominal Case.', ()=>{
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#\\36 66808451482405935 > :nth-child(5) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get(REPLACEMENT.ADD_BTN).should('be.visible').click()
         cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
         .invoke('css','height','350px')
         cy.handleModal()

         cy.getIframeDom(REPLACEMENT.SEAR_BTN).click()
         cy.get(REPLACEMENT.SEAR_INPUT).type('BICICI{enter}')
         cy.get(REPLACEMENT.SEAR_RES).as('liste').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste').first().children().first().click()
            }
          })

          cy.getIframeDom(REPLACEMENT.IDEN).type('012')
          cy.getIframeDom(REPLACEMENT.NAME).type('VISA Replacement Fees')
          cy.getIframeDom(REPLACEMENT.AMOUNT).type('33.14')

    })

})
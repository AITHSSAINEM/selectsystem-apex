import { DASHBOARD_PAGE, CARD } from '../../loctors/dashboard'
describe('TEST DASHBOARD.', ()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        // going to dashboard.
        cy.get(DASHBOARD_PAGE.DASHBOARD_ITEM).click()
    })
    it('Test issuing dashbaord', ()=>{

        cy.get(DASHBOARD_PAGE.ISSUING_ITEM).click()
        cy.get('#B760460883569079214 > .t-Button-label > b').click();
        cy.get(':nth-child(1) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(2) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(3) > .t-BadgeList-wrap').should('be.visible');
    })

    
    it('Test acquiring dashoard ', function() { 
        cy.get('#B760460982301079215 > .t-Button-label > b').click();
        cy.get(':nth-child(1) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(2) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(3) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(4) > .t-BadgeList-wrap').should('be.visible');
        
    });

    it('Test Switch dashboard' , ()=>{
        cy.get(':nth-child(1) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(2) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(3) > .t-BadgeList-wrap').should('be.visible');
    })
    it('Test Schemes dashboard', ()=>{
        cy.get('#B760461150560079217 > .t-Button-label > b').click();
        cy.get(':nth-child(1) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(2) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(3) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(4) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(5) > .t-BadgeList-wrap').should('be.visible');
    })
    it('Test Administration dashboard', ()=>{
        
        cy.get('#B760461206923079218 > .t-Button-label > b').click();
        cy.get('#R3273777089161456897 > .t-Region-bodyWrap > .t-Region-body').should('be.visible');
        cy.get(':nth-child(1) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(2) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(3) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(4) > .t-BadgeList-wrap').should('be.visible');
        cy.get(':nth-child(5) > .t-BadgeList-wrap').should('be.visible');
        
    })
})
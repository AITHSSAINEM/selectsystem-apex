/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Value Added Tax (VAT)',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get('#\\35 22400360937447804 > li:nth-child(3) > a').click()
        
    })

    it('Add VAT Record - Nominal case.', ()=>{
        cy.get('#myIG_ig_grid_vc_cur').click()
        cy.get('#mybtn2').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','400px')

        cy.getIframeDom('#P900112_VRE_RATE').type('20')
        cy.getIframeDom('#P900112_VRE_DATE_input').type('2022-10-10')
        cy.getIframeDom('#P900112_VRE_EXPO').type('2')
        cy.getIframeDom('#B238073621547175480').should('be.visible').click()
    })

    it('Add VAT Record -mandatory fields left empty- Exptional case.', ()=>{
        cy.get('#myIG_ig_grid_vc_cur').click()
        cy.get('#mybtn2').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','400px')

        cy.getIframeDom('#P900112_VRE_RATE').clear()
        cy.getIframeDom('#P900112_VRE_DATE_input')
        cy.getIframeDom('#P900112_VRE_EXPO').clear()
        

    })

    it('Add VAT Record- Invalid date  - Exptional case.', ()=>{
        cy.get('#myIG_ig_grid_vc_cur').click()
        cy.get('#mybtn2').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','400px')

        cy.getIframeDom('#P900112_VRE_RATE').type('20')
        cy.getIframeDom('#P900112_VRE_DATE_input').type('10-10-2022')
        cy.getIframeDom('#P900112_VRE_EXPO').type('2')
        cy.getIframeDom('#B238073621547175480').should('be.visible').click()
        cy.getIframeDom('#P900112_VRE_DATE_error').should('be.visible').contains('Date must be a valid date, for example 2023-05-03.')
    })
})
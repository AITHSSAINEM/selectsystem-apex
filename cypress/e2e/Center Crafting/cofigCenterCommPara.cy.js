
/// <reference types="cypress"/>
import {MENU} from '../../loctors/menu'
import { NEW_CENTER } from '../../loctors/center'
describe('Center Crafting.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    })

    it('Configure center commission parameters. - Nominal case ', ()=>{
        cy.get('#\\35 23400178171740781 > li:nth-child(1) > a').click()

        cy.get('#R186829090186608892_tab > a > span').click();
        cy.get('#R186829090186608892 > .t-Region-bodyWrap > .t-Region-body > .container').click();

        cy.get('#P900214_CTY_TRAN_CODE').clear();
        cy.get('#P900214_CTY_TRAN_CODE').type('4');
        cy.get('#P900214_CTY_MCC_GROU').clear('12');
        cy.get('#P900214_CTY_MCC_GROU').type('1');
        cy.get('#P900214_CTY_PCA_CODE').clear();
        cy.get('#P900214_CTY_PCA_CODE').type('2');
        cy.get('#P900214_CTY_CARD_USAG').clear();
        cy.get('#P900214_CTY_CARD_USAG').type('3');
        cy.get('#P900214_CTY_MAIL_ORDE_INTE_ORDE').clear();
        cy.get('#P900214_CTY_MAIL_ORDE_INTE_ORDE').type('6');
        cy.get('#P900214_CTY_COUN_GROU').clear();
        cy.get('#P900214_CTY_COUN_GROU').type('4');

        cy.get('#B129896481216358097').should('be.visible').click();
        
    })
})
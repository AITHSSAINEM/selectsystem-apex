/// <reference types='cypress'/>
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import {BRANCH} from "../../../loctors/bank/branch"
import {UI_DIALOG, FIRS_CONTACT, SEC_CONTACT} from '../../../loctors/center'
describe('branch- nominal case ', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        cy.viewport(1920, 1080)
        // Step 01 :  Access
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    });

    it('Test delete branch', ()=>{
      cy.get('#\\35 22408934932516876 > :nth-child(2) > .t-LinksList-link').click();
      cy.get('#myreg_ig_grid_vc_cur > a > .apex-edit-pencil-alt').click()
      cy.get('#B238277937348431769').should('be.visible');
      cy.get('#B238277937348431769 > .t-Button-label').should('have.text', 'Delete');
      // Pour supprimer une branche, il suffit d'activer la ligne suivante : 
      cy.get('#B238277937348431769').click()
      //cy.get('.js-confirmBtn').should('be.visible').click()
      
      
    })
})
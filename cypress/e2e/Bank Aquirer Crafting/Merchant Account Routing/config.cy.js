
/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Merchant Account Routing .',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    })
    it('Configure Merchant Account Routing ', ()=>{
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#\\35 23400178171740781 > :nth-child(2) > .t-LinksList-link').click();
        cy.get('#R182223024764852463_ig_grid_vc_cur > a > :nth-child(2)').click();
        cy.get('#B128788111905191878 > .t-Button-label').click();
        cy.get('#R164005221891253822_tab > a > span').click();
        cy.get('#P900212_MAR_SYST_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_MAR_TRAN_CODE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_MAR_CURR_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_MAR_SYST_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_MAR_TRAN_CODE_CONTAINER > .t-Form-inputContainer').click();
        cy.get('#P900212_MAR_TRAN_CODE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900212_MAR_CURR_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#B128814243583200840 > .t-Button-label').should('be.visible');
        /* ==== End Cypress Studio ==== */
    })
})
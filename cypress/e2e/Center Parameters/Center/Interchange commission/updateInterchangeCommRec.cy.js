import {MENU} from '../../../../loctors/menu'
import { NEW_CENTER} from '../../../../loctors/center'
describe('Center : Interchange commission',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
    })

    it(' Update interchange Commission  record- Nominal case ', ()=>{
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(3) > a').click()
        cy.get('#myreg_ig_grid_vc_cur > a').click()
        cy.get('#myreg_ig_grid_vc_cur > a').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height', '600px')
        cy.getIframeDom('#P100305_CRE_DEBI_CRED').select('D')
        cy.getIframeDom('#P100305_CRE_TRAN_CODE').select('07000')
        cy.getIframeDom('#P100305_CRE_PCA_CODE').select('1')
        cy.getIframeDom('#P100305_CRE_CCG_CODE').select('1')
        cy.getIframeDom('#P100305_CRE_CAU_CODE').select('C')
        cy.getIframeDom('#P100305_CRE_MAG_CODE').select('262')
        cy.getIframeDom('#P100305_CRE_MAIL_ORDE_INTE_ORDE').select('C')
        cy.getIframeDom('#P100305_CRE_COMM_FROM').clear().type('transaction')
        cy.getIframeDom('#P100305_CRE_RATE').clear().type('2')
        cy.getIframeDom('#P100305_CRE_FIXE').clear().type('12')
        cy.getIframeDom('#P100305_CRE_MIN').clear().type('10')
        cy.getIframeDom('#P100305_CRE_MAX').clear().type('30')
        cy.getIframeDom('#B837611862969927276').should('be.visible').and('have.text', 'Apply Changes').click()
    })
})
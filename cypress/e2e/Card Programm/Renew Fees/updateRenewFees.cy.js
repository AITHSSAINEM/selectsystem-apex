/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
describe('Renew Fees .',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Update New Fees -Nominal case ',()=>{

        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#\\36 66808451482405935 > :nth-child(4) > .t-LinksList-link > .t-LinksList-label').click();

        cy.get('#myreg_ig_grid_vc_cur > a').click()
  
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height', '450px')
        cy.handleModal()
        
        cy.getIframeDom('#P101007_CRF_BAN_CODE_lov_btn').click()
        cy.get('#PopupLov_101007_P101007_CRF_BAN_CODE_dlg > div.a-PopupLOV-searchBar > input')
        .type('BICICI{enter}')
        cy.get('#PopupLov_101007_P101007_CRF_BAN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste').first().children().first().click()
            }
          })
        cy.getIframeDom('#P101007_CRF_IDEN').clear().type('001')
        cy.getIframeDom('#P101007_CRF_LABE').clear().type('new Fees')
        cy.getIframeDom('#P101007_CRF_AMOU').clear().type('80')
        cy.getIframeDom('#B86744629322721102').should('be.visible').click()
        cy.get('#t_Alert_Success').should('be.visible')
      
    })


    it.only('Update Renew Fees - Mondatory fields left empty-Exptional case.', ()=>{
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#\\36 66808451482405935 > :nth-child(4) > .t-LinksList-link > .t-LinksList-label').click();

        cy.get('#myreg_ig_grid_vc_cur > a').click()
  
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height', '450px')
        cy.handleModal()
        
        cy.getIframeDom('#P101007_CRF_BAN_CODE_lov_btn').click()
        cy.get('#PopupLov_101007_P101007_CRF_BAN_CODE_dlg > div.a-PopupLOV-searchBar > input')
        .type('BICICI{enter}')
        cy.get('#PopupLov_101007_P101007_CRF_BAN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste').first().children().first().click()
            }
          })
        cy.getIframeDom('#P101007_CRF_IDEN').clear()
        cy.getIframeDom('#P101007_CRF_LABE').clear()
        cy.getIframeDom('#P101007_CRF_AMOU').clear()
        cy.getIframeDom('#B86744629322721102').should('be.visible').click()
        cy.getIframeDom('#t_Alert_Notification').should('be.visible')  
    })
})
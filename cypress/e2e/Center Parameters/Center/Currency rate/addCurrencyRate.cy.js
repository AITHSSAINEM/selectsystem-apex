/// <reference types="cypress"/>
import {MENU} from '../../../../loctors/menu'
import { NEW_CENTER , ADDRESS , UI_DIALOG, FIRS_CONTACT, SEC_CONTACT, CORRESPENDANCE } from '../../../../loctors/center'
describe('Center currency rate ',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
    })

    it(' Add currency rate  - Nominal case ', ()=>{
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#myreg_data_tab > a > span').should('be.visible').and('have.text','Center Menu').click()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(1) > a').click()
        cy.get('#B233931834794182929').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css' ,'height', '500px')
        cy.getIframeDom('#P900207_CCR_CUR_SECO_CODE_lov_btn').click()
        cy.get('#PopupLov_900207_P900207_CCR_CUR_SECO_CODE_dlg > div.a-PopupLOV-searchBar > input').clear()
        .type('978 - Euro{enter}')

        cy.get('#PopupLov_900207_P900207_CCR_CUR_SECO_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get("#PopupLov_900207_P900207_CCR_CUR_SECO_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul").first().children().first().click()
            }
          })

        cy.getIframeDom('#P900207_CCR_SYS_CODE').select('2')
        cy.getIframeDom('#P900207_CCR_EFFE_DATE_input').type('10-10-2022')
        cy.getIframeDom('#P900207_CCR_INTG_DATE_input').type('08-10-2022')
        cy.getIframeDom('#P900207_CCR_RATE').type('1.1')
        cy.getIframeDom('#B233921759724182924').should('be.visible').and('have.text','Create').click()
    })

    it(' Add currency rate  - Exceptional case ', ()=>{
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#myreg_data_tab > a > span').should('be.visible').and('have.text','Center Menu').click()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(1) > a').click()
        cy.get('#B233931834794182929').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css' ,'height', '500px')
        cy.getIframeDom('#B233921759724182924').should('be.visible').and('have.text','Create').click()
        cy.getIframeDom('#t_Alert_Notification > div').should('be.visible')
        cy.getIframeDom('#P900207_CCR_CUR_SECO_CODE_error').should('have.text', 'Currency must have some value.')
        cy.getIframeDom('#P900207_CCR_SYS_CODE_error').should('have.text', 'System must have some value.')
        cy.getIframeDom('#P900207_CCR_EFFE_DATE_error').should('have.text', 'Effective Date must have some value.')
        cy.getIframeDom('#P900207_CCR_INTG_DATE_error').should('have.text', 'Integration Date must have some value.')
        cy.getIframeDom('#P900207_CCR_RATE_error').should('have.text', 'Rate must have some value.')
    })
    
    it(' Add currency rate - Integration Date Greater than Effective Date  - Exceptional case -', ()=>{
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#myreg_data_tab > a > span').should('be.visible').and('have.text','Center Menu').click()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(1) > a').click()
        cy.get('#B233931834794182929').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css' ,'height', '500px')
        cy.getIframeDom('#P900207_CCR_CUR_SECO_CODE_lov_btn').click()
        cy.get('#PopupLov_900207_P900207_CCR_CUR_SECO_CODE_dlg > div.a-PopupLOV-searchBar > input').clear()
        .type('978 - Euro{enter}')

        cy.get('#PopupLov_900207_P900207_CCR_CUR_SECO_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get("#PopupLov_900207_P900207_CCR_CUR_SECO_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul").first().children().first().click()
            }
          })

        cy.getIframeDom('#P900207_CCR_SYS_CODE').select('2')
        cy.getIframeDom('#P900207_CCR_EFFE_DATE_input').type('10-10-2022')
        cy.getIframeDom('#P900207_CCR_INTG_DATE_input').type('25-12-2022')
        cy.getIframeDom('#P900207_CCR_RATE').type('1.1')
        cy.getIframeDom('#B233921759724182924').should('be.visible').and('have.text','Create').click()
        cy.getIframeDom('#t_Alert_Notification').should('be.visible')
        cy.getIframeDom('#t_Alert_Notification > div > div.t-Alert-content > div > div').contains('Effective Date must be greater or equal to Integration Date')
    })
})
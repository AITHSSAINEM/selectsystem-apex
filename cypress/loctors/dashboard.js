export const DASHBOARD_PAGE = {
    DASHBOARD_ITEM  : '#t_MenuNav_4i',
    ISSUING_ITEM    : '#B760460883569079214 > .t-Button-label > b',
    CARD_SECTION    : ':nth-child(1) > .t-BadgeList-wrap',
    CARDS_TOTAL     : ':nth-child(1) > .t-BadgeList-wrap > .t-BadgeList-value',
    ACCOUNT_SECTION : ':nth-child(2) > .t-BadgeList-wrap',
    ACCOUNTS_TOTAL  : ':nth-child(2) > .t-BadgeList-wrap > .t-BadgeList-value',
    CUSTOMER_SECTION: ':nth-child(3) > .t-BadgeList-wrap',
    CUSTOMERS_TOTAL : ':nth-child(3) > .t-BadgeList-wrap > .t-BadgeList-value',
    
}
  
export const CARD ={
    CARDS_TAB       : '#R894211846804282716_tab > a > span',
    CARDS_SATATISTICS_SECTION : '#R894211846804282716 > .t-Region-bodyWrap > .t-Region-body',
    BANK_FIELD      : "#P800100_BANK_CARDS",
    GROUP_BY_BANK : '#P800100_CARD_OPTIONS_1 > .apex-item-grid > .apex-item-grid-row > :nth-child(1) > .u-radio',
    GROUP_BY_TYPE :'#P800100_CARD_OPTIONS_1 > .apex-item-grid > .apex-item-grid-row > :nth-child(5) > .u-radio',
    DIAGRAM  : '#R894211846804282716_jet > svg:nth-child(4) > g'
}

export const CARD_RENEW ={
    CARDS_RENEW_TAB : '#R932216311939646022_tab > a > span',
    TITLE : '#R932216311939646022_tab > a > span',
    CARDS_RENEW_DASHBOARD : '#R932216311939646022 > .t-Region-bodyWrap > .t-Region-body',
    BANK_CARDS_TO_RENEW : '#P800100_BANK_CARDS_TO_RENEW',
    CARD_TYPE_CARDS_TO_RENEW :'#P800100_CARD_TYPE_CARDS_TO_RENEW',
    GROUP_BY_RENEWAL_MODE : '#P800100_CARD_TO_RENEW_SN > .apex-item-grid > .apex-item-grid-row > :nth-child(4) > .u-radio',
    RENEWAL_MODE_DIAGRAM : '#R932216311939646022_jet > svg:nth-child(4) > g > g > g > g:nth-child(2) > rect'
}

export const CARDS_CREATION_EVOLUTION ={

}
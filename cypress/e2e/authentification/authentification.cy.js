/// <reference types="cypress"/>

describe('Login', () => {
  it('Should login to the application with valid credentials.', () => {
    cy.login()
    cy.url().should('include', 'home')
  })
  
  it("Should not allow login without valid credentials.", ()=>{
    cy.login2('mohamed', 'mohamed@123')
    cy.get('#t_Alert_Notification').should('be.visible')
  })

  it('Should redirect the user to the login page after a successful logout.' ,()=>{
    cy.login()
    cy.url().should('include', 'home')
    cy.get('#USER > :nth-child(1) > .dropbtn').invoke('show');
    cy.contains('Sign Out').parent().invoke('show')
    cy.contains('Sign Out').scrollIntoView().should('be.visible')
    cy.contains('Sign Out').click()
    cy.url().should('include', 'connection')
  })
})

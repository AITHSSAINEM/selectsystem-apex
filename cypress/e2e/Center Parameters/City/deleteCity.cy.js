/// <reference types ="cypress"/>
import {MENU} from '../../../loctors/menu'
import {CITY} from '../../../loctors/city'
import { NEW_CENTER}from '../../../loctors/center'
describe('CITY ', ()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
    })
    it('Delete city.',()=>{
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get('#\\35 23404071394758395 > :nth-child(2) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get(CITY.PENCIL_ICON).click();
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height', '310px')
        cy.handleModal()
        cy.getIframeDom(CITY.DELETE_BTN).should('be.visible').click()
        cy.get('.js-confirmBtn').should('have.text', 'Delete').click()
        cy.get('#t_Alert_Success').should('be.visible')
    })
})
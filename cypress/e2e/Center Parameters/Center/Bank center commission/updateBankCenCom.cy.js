/// <reference types="cypress"/>
import {MENU} from '../../../../loctors/menu'
import { NEW_CENTER , ADDRESS , UI_DIALOG, FIRS_CONTACT, SEC_CONTACT, CORRESPENDANCE } from '../../../../loctors/center'
describe('Bank Center Commission',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
    })

    it('Add Bank Center Commission   - Nominal case ', ()=>{
        cy.get('#B538042368824713212').click()
        cy.handleModal()
        cy.getIframeDom('#\\32 32845273338252947 > li:nth-child(4) > a').click()
        cy.get('#R235103405906285701_ig_grid_vc > div.a-GV-bdy > div.a-GV-w-scroll > table > tbody > tr:nth-child(2) > td.a-GV-cell.u-tC.is-readonly > a')
        .click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','400px')

        cy.getIframeDom('#P900221_BAN_ACQU_COM_CODE').select('141')
        cy.getIframeDom('#P900221_BAN_ISSU_COM_CODE').select('141')
        cy.getIframeDom('#P900221_BAN_ISSU_ACQU_COM_CODE').select('141')
        cy.getIframeDom('#B235101274300285699').should('be.visible').and('have.text', 'Apply Changes').click()
        cy.get('#t_Alert_Success').should('be.visible')
    })
})

/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Value Added Tax (VAT)',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get('#\\35 22400360937447804 > li:nth-child(3) > a').click()
        
    })

    it('Update vat Record. - Nominal case ', ()=>{
        cy.get('#myregion_ig_grid_vc > div.a-GV-bdy > div.a-GV-w-scroll > table > tbody > tr > td.a-GV-cell.u-tC.is-readonly > a').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','350px')
        cy.getIframeDom('#P900112_VRE_RATE').clear().type('22')
        cy.getIframeDom('#P900112_VRE_DATE_input').clear().type('2022-12-12')
        cy.getIframeDom('#P900112_VRE_EXPO').clear().type('3')
        cy.getIframeDom('#B238073223664175480').should('be.visible').click()
        cy.get('#t_Alert_Success').should('be.visible')
    })
})
/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Value Added Tax (VAT)',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get('#\\35 22400360937447804 > li:nth-child(3) > a').click()
        
    })

    it('Delete vat. - Nominal case ', ()=>{
        cy.get('#myIG_ig_grid_vc_cur > a').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height', '300px')
        cy.getIframeDom('#B238045768741157573').should('be.visible').click()
        cy.get('.js-confirmBtn').should('be.visible').and('have.text', 'Delete').click()
        cy.get('#t_Alert_Success').should('be.visible')
    })
})
import {MENU} from '../../../loctors/menu'
describe('Status Management.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Change Card Status Configuration-Nominal case ',()=>{
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link').click();
        cy.get('#\\36 66871638801445837 > :nth-child(8) > .t-LinksList-link > .t-LinksList-label').click();


        /* ==== Generated with Cypress Studio ==== */
        cy.get('#R552269814686206415_tab > a > span').should('be.visible');
        cy.get('#R552269814686206415_tab > a > span').click();
        cy.get('#R552269814686206415_tab > a > span').should('be.visible');
        cy.get('#P101112_ACCOUNT_STATUS_CONTAINER > .t-Form-inputContainer').click();
        cy.get('#P101112_ACCOUNT_STATUS').select('Active');
        /* ==== End Cypress Studio ==== */
    })
})
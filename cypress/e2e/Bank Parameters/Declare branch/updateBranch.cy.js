/// <reference types='cypress'/>
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import {BRANCH} from "../../../loctors/bank/branch"
import {UI_DIALOG, FIRS_CONTACT, SEC_CONTACT} from '../../../loctors/center'
describe('branch- nominal case ', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        cy.viewport(1920, 1080)
        // Step 01 :  Access
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    });

    it('Test update branch', ()=>{
      cy.get('#\\35 22408934932516876 > :nth-child(2) > .t-LinksList-link').click();
      cy.get('#myreg_ig_grid_vc_cur > a > .apex-edit-pencil-alt').click()
      cy.get('#R238392453648911831_tab > a > span').click()
      cy.get('#P902205_BRA_IDEN_CONTAINER > .t-Form-inputContainer').click();
      cy.get('#P902205_BRA_IDEN').click();
      cy.get('#P902205_BRA_CORP_NAME').click();
      cy.get('#P902205_BRA_INIT').click();
      cy.get('#B238278331587431769').should('be.visible').and('have.text', 'Apply Changes').click();
      
      
    })

})
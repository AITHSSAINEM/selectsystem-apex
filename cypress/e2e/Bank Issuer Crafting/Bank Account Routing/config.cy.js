
/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Bank Account Routing.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    })

    it('Configure Bank Account Routing. - Nominal case.', ()=>{
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#\\35 23400178171740781 > :nth-child(2) > .t-LinksList-link').click();
        cy.get('#R182223024764852463_ig_grid_vc_cur > a > :nth-child(2)').click();
        cy.get('#B128788505300191878 > .t-Button-label').click();
        cy.get('#R376483102221043334_tab > a > span').click();
        cy.get('#P900213_BAR_TRAN_CODE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900213_BAR_CURR_CODE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        /* ==== End Cypress Studio ==== */
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#R169962351472901867_tab > a > span').click();
        cy.get('#P900213_NAT_BIN').clear('1');
        cy.get('#P900213_NAT_BIN').type('1');
        cy.get('#P900213_NAT_APP_CRT').clear();
        cy.get('#P900213_NAT_APP_CRT').type('2');
        cy.get('#P900213_TYPE_TRS').clear();
        cy.get('#P900213_TYPE_TRS').type('3');
        cy.get('#P900213_NAT_PIN').clear();
        cy.get('#P900213_NAT_PIN').type('4');
        cy.get('#R169962572443901869_tab > a > span').click();
        cy.get('#P900213_INT_ASSO').clear('1');
        cy.get('#P900213_INT_ASSO').type('1');
        cy.get('#P900213_INT_TYP_CARTE').clear();
        cy.get('#P900213_INT_TYP_CARTE').type('2');
        cy.get('#P900213_INT_APPLICATION_CRT').clear();
        cy.get('#P900213_INT_APPLICATION_CRT').type('3');
        cy.get('#P900213_INT_TYPE_TRS').clear();
        cy.get('#P900213_INT_TYPE_TRS').type('4');
        cy.get('#P900213_INT_PIN').clear();
        cy.get('#P900213_INT_PIN').type('5');
        cy.get('#B129773031398124417 > .t-Button-label').should('be.visible');
        /* ==== End Cypress Studio ==== */
    })
})
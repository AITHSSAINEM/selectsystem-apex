import {MENU} from '../../../loctors/menu'
describe('Insurance Fees.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Add Insurance Fees -Nominal case ',()=>{
        
        cy.get('#\\31 36860343944031158 > :nth-child(2) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#\\38 09001240334560682 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('.t-Button-label').click();

        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height', '450px') 
        cy.handleModal()   

         /************************************************************************************** */
         cy.getIframeDom('#P110202_DIF_BAN_CODE_lov_btn').click()
         cy.get('#PopupLov_110202_P110202_DIF_BAN_CODE_dlg > div.a-PopupLOV-searchBar > input')
         .type('BICICI{enter}')
         cy.get('#PopupLov_110202_P110202_DIF_BAN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste1').first().then(($ul) => {
             // Vérifiez si l'élément ul existe
             if ($ul.length) {
               // Sélectionnez le premier enfant de ul
               cy.get('@liste1').first().children().first().click()
             }
           })
         /************************************************************************************** */
         cy.getIframeDom('#P110202_DIF_PER_CODE_lov_btn').click()
         cy.get('#PopupLov_110202_P110202_DIF_PER_CODE_dlg > div.a-PopupLOV-searchBar > input')
         .type('Annuel{enter}')
         cy.get('#PopupLov_110202_P110202_DIF_PER_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste2').first().then(($ul) => {
             // Vérifiez si l'élément ul existe
             if ($ul.length) {
               // Sélectionnez le premier enfant de ul
               cy.get('@liste2').first().children().first().click()
             }
           })

            /************************************************************************************** */
          cy.getIframeDom('#P110202_DIF_ITY_CODE_lov_btn').click()
          cy.get('#PopupLov_110202_P110202_DIF_ITY_CODE_dlg > div.a-PopupLOV-searchBar > input')
          .type('test{enter}')
          cy.get('#PopupLov_110202_P110202_DIF_ITY_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste3').first().then(($ul) => {
              // Vérifiez si l'élément ul existe
              if ($ul.length) {
                // Sélectionnez le premier enfant de ul
                cy.get('@liste3').first().children().first().click()
              }
            })
        cy.getIframeDom('#P110202_DIF_IDEN').type('001')
        cy.getIframeDom('#P110202_DIF_LABE').type('VISA Insurance Fees')
        cy.getIframeDom('#P110202_DIF_AMOU').type('84')

        cy.getIframeDom('#B277909016007270905').should('be.visible').click()
    })
})

/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Cardholder Account Routing.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    })

    it('Configure Cardholder Account Routing. - Nominal case.', ()=>{
        cy.get('#\\35 23400178171740781 > :nth-child(2) > .t-LinksList-link').click();
        cy.get('#R182223024764852463_ig_grid_vc_cur > a > :nth-child(2)').click();
        cy.get('#R163513955837426405_tab > a > span').click();
        cy.get('#P900211_CAR_CURR_CODE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_CAR_MCC_GROU_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_CAR_CURR_CODE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_CAR_TRAN_CODE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_CAR_MCC_GROU_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_CAR_PAN_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#B128787068630191877').should('be.visible');

         // cy.get('#B128787068630191877').click()
        
    })
})
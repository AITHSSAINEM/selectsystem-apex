/// <reference types='cypress'/>
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import {BRANCH} from "../../../loctors/bank/branch"
import {UI_DIALOG, FIRS_CONTACT, SEC_CONTACT} from '../../../loctors/center'
describe('branch- nominal case ', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        cy.viewport(1920, 1080)
        // Step 01 :  Access
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    });

    it('Test add new branch',()=>{
       
        cy.get('#\\35 22408934932516876 > :nth-child(2) > .t-LinksList-link').click();
        cy.get('.t-Button-label').click();
        cy.get(BRANCH.IDEN).type('11542');
        cy.get(BRANCH.NAME).type('BICICI North');
        cy.get(BRANCH.INIT).type('BICICIN');
        cy.get(BRANCH.BANK).select('20');
        cy.get(BRANCH.CREATE_BTN).should('be.visible');

        // pour creer un une nouvelle branche, il suffit d'activer la ligne suivante : 
        //cy.get('#B238278738036431769').click()
        
    })

    /* ==== Test Created with Cypress Studio ==== */
    it('Test add branch\'s address', function() {
      /* ==== Generated with Cypress Studio ==== */
      cy.get('#\\35 22408934932516876 > :nth-child(2) > .t-LinksList-link > .t-LinksList-icon > .t-Icon').click();
      cy.get('#myreg_ig_grid_vc_cur > a > .apex-edit-pencil-alt').click();
      cy.get('#R888960779643684271_tab > a > span').click();
      cy.get('#B257735832646141732').click();
      /* ==== End Cypress Studio ==== */


      cy.get("#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable").invoke('css', 'height', '800px')
      cy.handleModal()

      cy.getIframeDom(UI_DIALOG.TYPE_BTN).click()
      cy.get(UI_DIALOG.TYPE_INP).type("01 - Mail Address{enter}")
      cy.get(UI_DIALOG.TYPE_LIST).first().then(($ul) => {
        // Vérifiez si l'élément ul existe
        if ($ul.length) {
          // Sélectionnez le premier enfant de ul
          cy.get(UI_DIALOG.TYPE_LIST).first().children().first().click()
        }
      })
      cy.getIframeDom(UI_DIALOG.ADDRESS).type('103 Zerktouni Avenue')
      cy.getIframeDom(UI_DIALOG.ADDRESS2).type('5th floor, Door 5')
      cy.getIframeDom(UI_DIALOG.STREET).type('Abidjan Street')
      cy.getIframeDom(UI_DIALOG.PO_BOX).type('54646')
      cy.getIframeDom(UI_DIALOG.ZIP).type('45100')
      cy.getIframeDom(UI_DIALOG.CITY_BTN).click()
      cy.get(UI_DIALOG.CITY_INP).type("Abidjan{enter}")
      cy.get(UI_DIALOG.CITY_LIST).first().then(($ul) => {
        // Vérifiez si l'élément ul existe
        if ($ul.length) {
          // Sélectionnez le premier enfant de ul
          cy.get(UI_DIALOG.CITY_LIST).first().children().first().click()
        }
      })
      cy.getIframeDom(UI_DIALOG.STATE).type('384')
      cy.getIframeDom(UI_DIALOG.PHONE1).type('0198765432')
      cy.getIframeDom(UI_DIALOG.PHONE2).type('0198765473')
      cy.getIframeDom(UI_DIALOG.FAX).type('0150765444')
      cy.getIframeDom('#R225546316643469027').should('be.visible')
      // remplir les champs des deux contacts.
      
      // First Conctact 

      cy.getIframeDom(FIRS_CONTACT.FIRS_NAME).type('Ahmed')
      cy.getIframeDom(FIRS_CONTACT.LAST_NAME).type('Ali')
      cy.getIframeDom(FIRS_CONTACT.FUNCTION).type('Agent')
      cy.getIframeDom(FIRS_CONTACT.PHONE).type('0165478498')
      cy.getIframeDom(FIRS_CONTACT.EMAIL).type('ahmed.ali@bicici.ci')
     
      // Second Contact
      cy.getIframeDom(SEC_CONTACT.FIRS_NAME).type('Mostapha')
      cy.getIframeDom(SEC_CONTACT.LAST_NAME).type('Mohamed')
      cy.getIframeDom(SEC_CONTACT.FUNCTION).type('Agent')
      cy.getIframeDom(SEC_CONTACT.PHONE).type('0165478498')
      cy.getIframeDom(SEC_CONTACT.EMAIL).type('m.mohamed@bicici.ci')
      cy.getIframeDom(UI_DIALOG.CREATE_BTN).should('be.visible')
    });


    it('Test update branch', ()=>{
      cy.get('#\\35 22408934932516876 > :nth-child(2) > .t-LinksList-link').click();
      cy.get('#myreg_ig_grid_vc_cur > a > .apex-edit-pencil-alt').click()
      cy.get('#R238392453648911831_tab > a > span').click()
      cy.get('#P902205_BRA_IDEN_CONTAINER > .t-Form-inputContainer').click();
      cy.get('#P902205_BRA_IDEN').click();
      cy.get('#P902205_BRA_CORP_NAME').click();
      cy.get('#P902205_BRA_INIT').click();
      cy.get('#B238278331587431769 > .t-Button-label').should('be.visible');
      cy.get('#B238278331587431769 > .t-Button-label').should('have.text', 'Apply Changes');
      cy.get('#B238278331587431769 > .t-Button-label').click();
      
    })

    it('Test delete branch', ()=>{
      cy.get('#\\35 22408934932516876 > :nth-child(2) > .t-LinksList-link').click();
      cy.get('#myreg_ig_grid_vc_cur > a > .apex-edit-pencil-alt').click()
      cy.get('#B238277937348431769').should('be.visible');
      cy.get('#B238277937348431769 > .t-Button-label').should('have.text', 'Delete');
      // Pour supprimer une branche, il suffit d'activer la ligne suivante : 
      //cy.get('#B238277937348431769').click()
      
      
    })
})
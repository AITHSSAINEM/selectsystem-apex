import {MENU} from '../../../loctors/menu'
describe('Membership Fees.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()

    })

    it('Add Membership Fees -Nominal case ',()=>{
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link').click();
        cy.get('#\\36 66808451482405935 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('.t-Button-label').click();
        

        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height', '450px')
        cy.handleModal()
        cy.getIframeDom('#P101015_CMF_IDEN').type('842')
        cy.getIframeDom('#P101015_CMF_LABE').type('VISA Membership Fees')
        cy.getIframeDom('#P101015_CMF_AMOU').type('100')

        /** *********************************************************************************** */
        cy.getIframeDom('#P101015_CMF_PER_CODE_lov_btn').click()
        cy.get('#PopupLov_101015_P101015_CMF_PER_CODE_dlg > div.a-PopupLOV-searchBar > input')
        .type('annuel{enter}')
        cy.get('#PopupLov_101015_P101015_CMF_PER_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste1').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste1').first().children().first().click()
            }
          })

          /************************************************************************************** */
          cy.getIframeDom('#P101015_CMF_BAN_CODE_lov_btn').click()
          cy.get('#PopupLov_101015_P101015_CMF_BAN_CODE_dlg > div.a-PopupLOV-searchBar > input')
          .type('BICICI{enter}')
          cy.get('#PopupLov_101015_P101015_CMF_BAN_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste2').first().then(($ul) => {
              // Vérifiez si l'élément ul existe
              if ($ul.length) {
                // Sélectionnez le premier enfant de ul
                cy.get('@liste2').first().children().first().click()
              }
            })

            /*********************************************************************************** */

            cy.getIframeDom('#B87043394648351111').should('be.visible').click()
    
    })

    it('Add Membership Fees - Mondatory fiels left empty -Exceptional case ',()=>{
        cy.get('#\\31 36860343944031158 > :nth-child(1) > .t-LinksList-link').click();
        cy.get('#\\36 66808451482405935 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('.t-Button-label').click();
        

        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height', '450px')
        cy.handleModal()
        cy.getIframeDom('#B87043394648351111').should('be.visible').click()

        cy.getIframeDom('#t_Alert_Notification > div').should('be.visible')
        cy.getIframeDom('#P101015_CMF_IDEN_error').should('be.visible')
        cy.getIframeDom('#P101015_CMF_AMOU_error').should('be.visible')
        cy.getIframeDom('#P101015_CMF_PER_CODE_error').should('be.visible')
        cy.getIframeDom('#P101015_CMF_BAN_CODE_error').should('be.visible')
        
    
    })
})
export const Commission = {
PENCIL : "#myreg_ig_grid_vc_cur > a > .apex-edit-pencil-alt",
LINK : "#R229880866326775420_tab > a",
BOTH : '#P902203_BAN_ISSU_ACQU_COM_CODE',
ISSUER :'#P902203_BAN_ISSU_COM_CODE',
ACQUIRER :"#P902203_BAN_ACQU_COM_CODE",
APPLY_CHANGES :'#B231837788870467778'
}

export const Revolving = {
LINK : "#R229881109427775423_tab > a", 
LAST_BUS_DATE_BTN : "#P902203_BAN_LAST_BUSI_DATE > .a-Button",
LAST_BUS_DATE_YEAR:"#P902203_BAN_LAST_BUSI_DATE_year",
LAST_BUS_DATE_MONTH :"#P902203_BAN_LAST_BUSI_DATE_month",
LAST_BUS_DATE_DAY :'[data-date="2022-10-25"] > span',
BUS_DATE_BTN : "#P902203_BAN_BUSI_DATE > .a-Button > .a-Icon",
BUS_DATE_YEAR : "#P902203_BAN_BUSI_DATE_year",
BUS_DATE_MONTH:"#P902203_BAN_BUSI_DATE_month",
BUS_DATE_DAY:'[data-date="2022-11-25"] > span',
APPLY_CHANGES :"#B231837788870467778"
}

export const Settlement = {
LINK : "#myregsac_tab > a",
ADD_ROW_BTN : "#mybtnsac > .t-Button-label",
IFRAME :"#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable",
MXP_ACCOUT: "#P902241_BSA_ACC_NUMB_MXP",
BANK_ACCOUNT: "#P902241_BSA_ACC_NUMB",
CURRENCY :"#P902241_BSA_ACC_CURR_CODE",
CREATE_BTN :"#B235330849282512133"
}
/// <reference types='cypress'/>
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import {SCHEME} from "../../../loctors/bank payment scheme/bankPaymentScheme"
describe('Bank currency rate - nominal case ', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        // Step 01 :  Access
        cy.viewport(1920, 1080)
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    });
    
    it('update bank\'s currency rate. -Nominal case.', ()=>{
        cy.get('#\\35 22408934932516876 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('.is-selected > :nth-child(2) > a > .fa').click();
        cy.handleModal()
        cy.getIframeDom('#\\32 35028513001019165 > li:nth-child(2) > a > span.t-LinksList-label').click()

        cy.get('#R598318356113698555_ig_grid_vc_cur > a').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','500px') 
        cy.getIframeDom('#P900227_BCC_RATE').clear().type('10')
        cy.getIframeDom('#P900227_BCC_INTG_DATE_input').clear().type('30-10-2008{enter}')
        cy.getIframeDom('#B364421650493536654').should('be.visible').click()
    })

    it('update bank\'s currency rate -Integration date greater than effective date-Exptional case.', ()=>{
        cy.get('#\\35 22408934932516876 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('.is-selected > :nth-child(2) > a > .fa').click();
        cy.handleModal()
        cy.getIframeDom('#\\32 35028513001019165 > li:nth-child(2) > a > span.t-LinksList-label').click()

        cy.get('#R598318356113698555_ig_grid_vc_cur > a').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css','height','500px') 
        cy.getIframeDom('#P900227_BCC_RATE').clear().type('10')
        let today = new Date();
        let day = today.getDate().toString().padStart(2, '0');
        let month = (today.getMonth() + 1).toString().padStart(2, '0'); // getMonth() returns a zero-based index
        let year = today.getFullYear().toString();
        let formattedDate = `${day}-${month}-${year}`;
        cy.getIframeDom('#P900227_BCC_INTG_DATE_input').clear().type(formattedDate+'{enter}')
        cy.getIframeDom('#B364421650493536654').should('be.visible').click()
        cy.getIframeDom('#t_Alert_Notification > div').should('be.visible')
        cy.getIframeDom('#t_Alert_Notification > div > div.t-Alert-content').should('be.visible').contains('Effective Date must be greater or equal to Integration Date')
    })

})
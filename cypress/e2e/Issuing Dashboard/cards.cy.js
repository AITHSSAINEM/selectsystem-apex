/// <reference types="cypress" />
import { DASHBOARD_PAGE, CARD } from '../../loctors/dashboard'
describe('Issuing dashoard - Cards -', ()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        // going to dashboard > issuing.
        cy.get(DASHBOARD_PAGE.DASHBOARD_ITEM).click()
        cy.get(DASHBOARD_PAGE.ISSUING_ITEM).click()
    })
    it('Cards - All Banks - group by "BANK"',()=>{
        cy.get(CARD.CARDS_TAB).click() 
        cy.get(CARD.CARDS_SATATISTICS_SECTION).should('be.visible')
        cy.get(CARD.BANK_FIELD).select('').contains('--ALL--')
        cy.get(CARD.GROUP_BY_BANK).click()
        cy.get(CARD.DIAGRAM).should('be.visible')
    })
    it('Cards - All Banks - group by "TYPE"',()=>{
        cy.get(CARD.CARDS_TAB).click() 
        cy.get(CARD.CARDS_SATATISTICS_SECTION).should('be.visible')
        cy.get(CARD.BANK_FIELD).select('').contains('--ALL--')
        cy.get(CARD.GROUP_BY_TYPE).click()
        cy.get(CARD.DIAGRAM).should('be.visible')
    })
    it('Cards - One Bank - group by "TYPE"',()=>{
        cy.get(CARD.CARDS_TAB).click() 
        cy.get(CARD.CARDS_SATATISTICS_SECTION).should('be.visible')
        cy.get(CARD.BANK_FIELD).select('20').contains('BICICI')
        cy.get(CARD.GROUP_BY_TYPE).click()
        cy.get(CARD.DIAGRAM).should('be.visible')
    })

    it('Cards - One Bank - group by "BANK"',()=>{
        cy.get(CARD.CARDS_TAB).click() 
        cy.get(CARD.CARDS_SATATISTICS_SECTION).should('be.visible')
        cy.get(CARD.BANK_FIELD).select('20').contains('BICICI')
        cy.get(CARD.GROUP_BY_BANK).click()
        cy.get(CARD.DIAGRAM).should('be.visible')
    })

    /* ==== Test Created with Cypress Studio ==== */
    it.only('Acquiring test', function() {
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#B760460982301079215 > .t-Button-label > b').click();
        cy.get(':nth-child(1) > .t-BadgeList-wrap > .t-BadgeList-value').should('be.visible');
        cy.get(':nth-child(2) > .t-BadgeList-wrap > .t-BadgeList-value').should('be.visible');
        cy.get(':nth-child(3) > .t-BadgeList-wrap').should('be.visible');
       // cy.get(':nth-child(4) > .t-BadgeList-wrap > .t-BadgeList-value').should('be.visible');
        /* ==== End Cypress Studio ==== */
    });
})
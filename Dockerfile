
# Utilisez une image de Node.js comme image de base
FROM node:18.15.0

# Définissez le répertoire de travail pour l'image Docker
WORKDIR /app

# Définition de la variable d'environnement pour le port
ENV PORT=3000

# Copiez les fichiers de votre application dans l'image Docker
COPY . .

# Copie des fichiers package.json et package-lock.json
COPY package*.json ./

# Installez les dépendances de l'application
RUN npm install

# Installez Cypress globalement
RUN npm install -g cypress@12.8.1

# Installez Chrome
RUN apt-get update && apt-get install -y \
    wget \
    gnupg \
    --no-install-recommends \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list \
    && apt-get update && apt-get install -y \
    google-chrome-stable \
    --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

# Exécutez les tests Cypress en mode headless
CMD ["npm", "run","test"]

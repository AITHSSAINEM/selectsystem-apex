/// <reference types='cypress'/>
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
import {SCHEME} from "../../../loctors/bank payment scheme/bankPaymentScheme"
describe('bank currency update - nominal case ', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        // Step 01 :  Access
        cy.viewport(1920, 1080)
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    });
    
    it('Test add bank currency update',()=>{
        cy.get('#\\35 22408934932516876 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('.is-selected > :nth-child(2) > a > .fa').click();
        cy.handleModal()
        cy.getIframeDom('#\\32 35028513001019165 > li:nth-child(1) > a').click();
        cy.get('#B303604396105397342').click();
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css' ,'height', '250px')
        cy.handleModal()
        cy.getIframeDom('#P902213_CUB_CUR_CODE_lov_btn').click()
        cy.get('#PopupLov_902213_P902213_CUB_CUR_CODE_dlg > div.a-PopupLOV-searchBar > input').type('CFA FRANC{enter}')
        cy.get('#PopupLov_902213_P902213_CUB_CUR_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get("#PopupLov_902213_P902213_CUB_CUR_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul").first().children().first().click()
            }
          })

        cy.getIframeDom('#B303598276567397339').should('be.visible').click()
    })

})

/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('issuer Risk Management level.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    })

    it('Configure isseur risk management level. - Nominal case.', ()=>{
     
        cy.get('#\\35 23400178171740781 > :nth-child(2) > .t-LinksList-link').click();
        cy.get('#R182223024764852463_ig_grid_vc_cur > a > :nth-child(2)').click();
        cy.get('#R163841875548230632_tab > a > span').click();
        cy.get('#P900211_ARL_CARD_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_ARL_ACCO_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_ARL_CUST_CONTAINER > .t-Form-inputContainer').click();
        cy.get('#P900211_ARL_CUST_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_ARL_ACCO_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_ARL_CARD_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#B128787068630191877').should('be.visible');
        cy.get('#B128787068630191877 > .t-Button-label').should('have.text', 'Apply Changes');

        // pour enregister les modufications, il suffit d'activer la ligne suivante : 

        //cy.get('#B128787068630191877').click()

        
    })
})
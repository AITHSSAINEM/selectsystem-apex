/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('Center : Payment Scheme.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    })


    it('Update center payment scheme - nominal case.', ()=>{
        cy.get('#\\35 22400360937447804 > :nth-child(2) > .t-LinksList-link > .t-LinksList-label').click()
        cy.get('#R229108889603451129_ig_grid_vc > div.a-GV-bdy > div.a-GV-w-scroll > table > tbody > tr:nth-child(4) > td.a-GV-cell.u-tC.is-readonly > a')
        .click()
        cy.get('#R229723331617574712_tab > a ').click()
        cy.get('#P900205_SYS_CORP_IDEN').clear().type('CTMI')
        cy.get('#P900205_SYS_CORP_NAME').clear().type('CTMI')
        cy.get('#P900205_SYS_INIT').clear().type('CTMI')

        cy.get('#R229723456989574713_tab > a ').click()
        cy.get('#P900205_SYS_SETT_CURR_CODE_lov_btn').click()
        cy.get('#PopupLov_900205_P900205_SYS_SETT_CURR_CODE_dlg > div.a-PopupLOV-searchBar > input')
        .clear().type('Dji{enter}')
        cy.get('#PopupLov_900205_P900205_SYS_SETT_CURR_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get('@liste').first().children().first().click()
            }
          })
        cy.get('#P900205_SYS_ACC_NUMB_MXP').clear().type('14')
        cy.get('#P900205_SYS_ACC_NUMB').clear().type('20')

        cy.get('#R229723687943574715_tab > a > span').click()

       
        cy.get('#P900205_SYS_ACQU_IDEN').clear().type('123456');
        cy.get('#P900205_SYS_ACQU_BUSI_IDEN').clear();
        cy.get('#P900205_SYS_ACQU_BUSI_IDEN').type('654321');
        cy.get('#P900205_SYS_ICA_CODE').clear();
        cy.get('#P900205_SYS_ICA_CODE').type('123456');
        cy.get('#P900205_SYS_PROC_ID').clear();
        cy.get('#P900205_SYS_PROC_ID').type('654321');
        cy.get('#P900205_SYS_REG_CODE').select('2');
        cy.get('#P900205_SYS_DOM_CODE').select('N');

        cy.get('#B229106648456451119').should('be.visible').and('have.text', 'Apply Changes').click()
        cy.get('#t_Alert_Success').should('be.visible')
    })

    it('Update center payment scheme - mandatory fields left empty - Exptional case .', ()=>{
        cy.get('#\\35 22400360937447804 > :nth-child(2) > .t-LinksList-link > .t-LinksList-label').click()
        cy.get('#R229108889603451129_ig_grid_vc > div.a-GV-bdy > div.a-GV-w-scroll > table > tbody > tr:nth-child(4) > td.a-GV-cell.u-tC.is-readonly > a')
        .click()
        cy.get('#R229723331617574712_tab > a ').click()
        cy.get('#P900205_SYS_CORP_IDEN').clear()
        cy.get('#R229723456989574713_tab > a ').click()
        cy.get('#P900205_SYS_ACC_NUMB_MXP').clear()
        cy.get('#P900205_SYS_ACC_NUMB').clear()
        cy.get('#R229723687943574715_tab > a > span').click()
        cy.get('#B229106648456451119').should('be.visible').and('have.text', 'Apply Changes').click()
      
        cy.get('#t_Alert_Notification').should('be.visible')
        cy.get('#t_Alert_Notification > div > div.t-Alert-content').contains('3 errors have occurred')
    })
})
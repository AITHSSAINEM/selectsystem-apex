
/// <reference types="cypress"/>
import {MENU} from '../../../loctors/menu'
import { NEW_CENTER } from '../../../loctors/center'
describe('issuer Risk Management Parameters.',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
    })

    it('Configure isseur risk management parameters. - Nominal case.', ()=>{
       
        cy.get('#\\35 23400178171740781 > :nth-child(2) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('#R182223024764852463_ig_grid_vc_cur > a > :nth-child(2)').click();
        cy.get('#R163514242659426408_tab > a > span').click();
        
        cy.get('#P900211_ARM_TRAN_CODE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_ARM_MAIL_ORDE_INTE_ORDE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_ARM_MERC_DOMA_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_ARM_TRAN_CODE_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#P900211_ARM_MERC_DOMA_CONTAINER > .t-Form-inputContainer > .t-Form-itemWrapper > .a-Switch > .a-Switch-toggle').click();
        cy.get('#B128787068630191877').should('be.visible');
        cy.get('#B128787068630191877').should('have.text', 'Apply Changes');
        // Pour enregister ces modifications, il suffit d'activer la ligne suivante : 
        //cy.get('#B128787068630191877').click()
    })
})
/// <reference types="cypress"/>
import {MENU} from '../../loctors/menu'
import { NEW_CENTER , ADDRESS , UI_DIALOG, FIRS_CONTACT, SEC_CONTACT, CORRESPENDANCE } from '../../loctors/center'
describe('Update Center',()=>{
    beforeEach(()=>{
        // Login to SelectSystem
        cy.login() 
        cy.get(MENU.SETTINGS).click()
        cy.get(NEW_CENTER.BASE_PARAM).click();
        cy.get(NEW_CENTER.CENTER_ITEM).click();
    })


    it.only('Test update center - Nominal case.', ()=>{
      cy.wait(100)
      cy.get('a[aria-controls="R631227054534542558"').click({force: true})
      let randomNum = Math.floor(Math.random() * 100);

      // Step 01 : CENTER 

      cy.get(NEW_CENTER.CENTER_ID).clear('');
      cy.get(NEW_CENTER.CENTER_NAME).clear('');
      cy.get(NEW_CENTER.CENTER_BTN_SEAR).click();
      cy.get(NEW_CENTER.CENTER_INP_SEAR).clear();

      /*cy.get(NEW_CENTER.CENTER_INP_SEAR).type('950 - CFA Franc BEAC{enter}');
      cy.get(NEW_CENTER.LIST_REF).first().then(($ul) => {
          // Vérifiez si l'élément ul existe
          if ($ul.length) {
            // Sélectionnez le premier enfant de ul
            cy.get(NEW_CENTER.LIST_REF).first().children().first().click()
          }
        })
     */
      cy.get('#B223995386427068629').should('be.visible').and('have.text', 'Apply Changes').click()

      cy.get('#P900201_CEN_IDEN_error').should('be.visible');
      cy.get('#P900201_CEN_IDEN_error').should('have.text', 'ID must have some value.');
      cy.get('.t-Alert-content').should('be.visible');
      
    })

})
/// <reference types='cypress'/>
import { MENU } from "../../../loctors/menu"
import { NEW_CENTER } from "../../../loctors/center";
describe(' Add Bank Card Generation Method- nominal case ', ()=>{
    beforeEach(()=>{
        //Step 00: login 
        cy.login()
        // Step 01 :  Access
        cy.viewport(1920, 1080)
        cy.get(MENU.SETTINGS).click({force:true})
        cy.get(NEW_CENTER.BASE_PARAM).click();
    });
    
    it('Test Add Bank Card Generation MethodSs',()=>{
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#\\35 22408934932516876 > :nth-child(1) > .t-LinksList-link > .t-LinksList-label').click();
        cy.get('.is-selected > :nth-child(2) > a > .fa').click();
        cy.handleModal()
        cy.getIframeDom('#\\32 35028513001019165 > li:nth-child(6) > a').click()
        cy.get('#B301835869492319428').click()
        cy.handleModal()
        cy.get('#t_PageBody > div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.ui-dialog--apex.t-Dialog-page--standard.ui-draggable')
        .invoke('css', 'height','250px')

        cy.getIframeDom('#P900225_BCG_CNG_CODE_lov_btn').click()
        cy.get('#PopupLov_900225_P900225_BCG_CNG_CODE_dlg > div.a-PopupLOV-searchBar > input').type('Serial Method{enter}')
        
        cy.get('#PopupLov_900225_P900225_BCG_CNG_CODE_dlg > div.a-PopupLOV-results.a-TMV > div > div.a-TMV-w-scroll > ul').as('liste').first().then(($ul) => {
            // Vérifiez si l'élément ul existe
            if ($ul.length) {
              // Sélectionnez le premier enfant de ul
              cy.get("@liste").first().children().first().click()
            }
          })

        cy.getIframeDom('#B301829719202319422').should('be.visible').click()
        
    })
})